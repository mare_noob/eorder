<?php
/**
 * Function for generating random strings
 */
if (!function_exists('randomStringGenerator')) {
    function randomStringGenerator($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[mt_rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
/**
 * Calculate total price considering trade dicount and vat
 */
// vat
if (!function_exists('calculateVat')) {
    function calculateVat($price, $vat)
    {
        $total = $price * $vat / 100;
        return $total;
    }
}
// trade discount
if (!function_exists('calculateTradeDiscount')) {
    function calculateTradeDiscount($price, $trade_discount)
    {
        $total = $price * $trade_discount / 100;
        return $total;
    }
}
/**
 * Converting date to Serbian date time format
 */
if (!function_exists('formatDate')) {
    function formatDate($string, $no_time = false)
    {   
        $formated_date = date('H:i:s d-m-Y', strtotime($string));
        if ($no_time) {
            $formated_date = date('d-m-Y', strtotime($string));
        }
        
        return $formated_date;
    }
}
