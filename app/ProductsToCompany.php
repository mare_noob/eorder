<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsToCompany extends Model
{
    public $timestamps = false;
    protected $table = 'products_to_companies';
}
