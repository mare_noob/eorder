<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public $timestamps = false;

    /**
     *
     * Show all data from table with pagination
     *
     * @param    int  $per_page paginator parameter how much results to display
     * @return   eloquent object
     *
     */
    public function read($per_page = 15)
    {

        $result = Company::paginate($per_page);
        return $result;
    }
    /**
     *
     * Show all data from table
     * @return   eloquent object
     *
     */
    public function getAllRows()
    {

        $result = Company::all();
        return $result;
    }

    /**
     *
     * Inserts data into table
     *
     * @param    array  $params data from html form
     * @return   void
     *
     */
    public function insertRow($params)
    {
        $company = new Company;
        foreach ($params as $key => $value) {
            $company->$key = $value;
        }
        $company->save();
    }
    /**
     *
     * Edit data in table
     *
     * @param    array  $params data from html form
     * @return   void
     *
     */
    public function editRow($params)
    {
        $company = Company::find($params['id']);
        foreach ($params as $key => $value) {
            $company->$key = $value;
        }
        $company->save();
    }
    /**
     *
     * Delete data from table
     *
     * @param    int  $id primary key in table
     * @return   void
     *
     */
    public function deleteRow($id)
    {
        $products = Product::where('company_id', '=', $id)->get();
        $users = User::where('company_id', '=', $id)->get();
        if (!$products->isEmpty()) {
            ProductsToCompany::where('company_id', '=', $id)->delete();
            Product::where('company_id', '=', $id)->delete();
        }
        if (!$users->isEmpty()) {
            User::where('company_id', '=', $id)->delete();
        }
        Company::find($id)->delete();
    }
    /**
     *
     * Searches data from table
     *
     * @param    string  $param string to search data in table
     * @param    string  $param string to search data in table
     * @return   eloquent object
     *
     */
    public function filterCompanies($param, $per_page = 15)
    {
        $rows = Company::where('name', 'like', '%' . $param . '%')->orWhere('address', 'like', '%' . $param . '%')->orWhere('phone', 'like', '%' . $param . '%')->paginate($per_page);

        return $rows;
    }
    /**
     *
     * Get reult by id
     *
     * @param    int  $id primary key in table
     * @return   eloquent object
     *
     */
    public function getCompanyById($id)
    {
        return Company::find($id);
    }
}
