<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'index';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view('/auth/login');
    }

    /**
     *  If something important needs to be done on server, prevent everyone except super admin to enter app
     */
    // protected function credentials(Request $request)
    // {
    //     return array_merge($request->only($this->username(), 'password'), ['admin_role' => 0]);
    // }

    // protected function sendFailedLoginResponse(Request $request)
    // {
    //     return redirect('under_construction');
    // }

}
