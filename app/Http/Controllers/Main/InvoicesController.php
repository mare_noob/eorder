<?php

namespace App\Http\Controllers\Main;

use App\Company;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
use Validator;

class InvoicesController extends Controller
{
    private $mdlInvoice;
    private $mdlCompany;
    private $mdlUser;
    private $mdlProduct;
    private $pageCustomJs;
    private $pageVendorJs;
    private $pageVendorCss;
    /**
     *  Load page specific and custom made js and css files
     */
    public function __construct()
    {
        $this->mdlInvoice = new Invoice;
        $this->mdlCompany = new Company;
        $this->mdlUser = new User;
        $this->mdlProduct = new Product;
        $this->pageCustomJs = [
            'js/invoices.js',
        ];
        $this->pageVendorJs = [
            'assets/vendor/pnotify/pnotify.custom.js',
            'assets/vendor/bootstrap-confirmation/bootstrap-confirmation.js',
            'assets/vendor/select2/js/select2.js',
            'assets/vendor/fuelux/js/spinbox.js',

        ];
        $this->pageVendorCss = [
            'assets/vendor/pnotify/pnotify.custom.css',
            'assets/vendor/select2/css/select2.css',
            'assets/vendor/fuelux/js/spinner.css',
        ];
    }

    public function index(Request $request)
    {//die(var_dump($request->input('company')));
        $company_search = $request->input('company');
        $search = '';
        $company_data = $this->mdlCompany->getAllRows();
        if (isset($company_search)) {
            $invoice_data = $this->mdlInvoice->filterInvoices($search, $company_search);
            $debt_paid_data = $this->mdlInvoice->calculateTotal($company_search);
            return view('main/invoices/invoices_index', [
                'invoice_data' => $invoice_data,
                'company_data' => $company_data,
                'debt_paid_data' => $debt_paid_data,
                'company_search' => $company_search,
                'pageCustomJs' => $this->pageCustomJs,
                'pageVendorJs' => $this->pageVendorJs,
                'pageVendorCss' => $this->pageVendorCss,
            ]);
        } else {
            $invoice_data = $this->mdlInvoice->read();
            $debt_paid_data = $this->mdlInvoice->calculateTotal('all');
            return view('main/invoices/invoices_index', [
                'invoice_data' => $invoice_data,
                'company_data' => $company_data,
                'debt_paid_data' => $debt_paid_data,
                'pageCustomJs' => $this->pageCustomJs,
                'pageVendorJs' => $this->pageVendorJs,
                'pageVendorCss' => $this->pageVendorCss,
            ]);
        }
        
        
    }
    // show create invoice page
    public function showCreateInvoices($invoice_id = NULL)
    {
        if ($invoice_id === NULL) {
            $user_id = Auth::user()->id;
            $user_data = $this->mdlUser->getUserById($user_id);
            $company_data = $this->mdlCompany->getAllRows();
            $product_data = $this->mdlProduct->getProductByCompanyId($user_data->company_id);
            $invoice_name = 'Faktura/otpremnica br - ' . date("d_m_Y") . '_' . randomStringGenerator(4);
            /**
             * Check if name with same value exists in invoices table,
             * if it does give new random name to $invoice_name
             */
            $invoice_all_data = $this->mdlInvoice->getAllRows();
            $name_exists = false;
            foreach ($invoice_all_data as $item) {
                if ($item->name === $invoice_name) {
                    $name_exists = true;
                }
            }
            if ($name_exists) {
                $invoice_name = 'Faktura/otpremnica br - ' . date("d_m_Y") . '_' . randomStringGenerator(4);
            }
            /** ************************************ */
            
            return view('main/invoices/invoices_create', [
                'company_data' => $company_data,
                'user_data' => $user_data,
                'invoice_name' => $invoice_name,
                'product_data' => $product_data,
                'pageCustomJs' => $this->pageCustomJs,
                'pageVendorJs' => $this->pageVendorJs,
                'pageVendorCss' => $this->pageVendorCss,
            ]);
        } else {
            $user_id = Auth::user()->id;
            $user_data = $this->mdlUser->getUserById($user_id);
            $invoice_data = $this->mdlInvoice->getInvoiceById($invoice_id);
            $order_data = $this->mdlInvoice->getOrderByInvoiceId($invoice_id);
            $company_data = $this->mdlCompany->getAllRows();
            $product_data = $this->mdlProduct->getProductByCompanyId($invoice_data->by_company_id);
            if ($user_data->admin_role <= $invoice_data->user_role ) {
                return view('main/invoices/invoices_create', [
                    'company_data' => $company_data,
                    'invoice_data' => $invoice_data,
                    'product_data' => $product_data,
                    'order_data' => $order_data,
                    'crud_type' => 'edit_invoice',
                    'pageCustomJs' => $this->pageCustomJs,
                    'pageVendorJs' => $this->pageVendorJs,
                    'pageVendorCss' => $this->pageVendorCss,
                ]);
            } else {
                return redirect('main/unauthorized');
            }
            
            
            
        }
        
    }

    public function viewInvoices($invoice_id)
    {
        $user_id = Auth::user()->id;
        $user_data = $this->mdlUser->getUserById($user_id);
        $company_data = $this->mdlCompany->getAllRows();
        $product_data = $this->mdlProduct->getProductByCompanyId($user_data->company_id);
        $invoice_data = $this->mdlInvoice->getInvoiceById($invoice_id);
        $orders_data = $this->mdlInvoice->getOrderByInvoiceId($invoice_id);
        return view('main/invoices/invoices_view', [
            'company_data' => $company_data,
            'user_data' => $user_data,
            'product_data' => $product_data,
            'invoice_data' => $invoice_data,
            'orders_data' => $orders_data,
            'pageCustomJs' => $this->pageCustomJs,
            'pageVendorJs' => $this->pageVendorJs,
            'pageVendorCss' => $this->pageVendorCss,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createInvoices(Request $request)
    {
        if ($request->ajax()) {
            Validator::make($request->input('formData'), [
                'total' => 'required',
            ])->validate();
            $form_data = collect($request->input('formData'))->except('_token');
            $orders_data = $request->input('ordersData');
            $this->mdlInvoice->insertRow($form_data, $orders_data);

            return response()->json(['success' => true], 200);
        }

    }

    /**
     * Update data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function put(Request $request)
    {
        if ($request->ajax()) {
            $invoice_data = collect($request->input('formData'))->except('_token');   
            /** ******
             * check if editing all fields or just status
             *******/
            if ($request->input('edit_type') === 'status') {
                             
                $this->mdlInvoice->editRow($invoice_data, 'status');
                return response()->json(['success' => true, 200]);
            } else {
                $orders_data = $request->input('ordersData');
                

                /* -------------------------------------------
                 * in order to ignore existing id for validation
                 * proper format is:
                 * unique:table,column,except,idColumn
                 */
                Validator::make($request->input('formData'), [
                    'total' => 'required',
                ])->validate();
                    
                $this->mdlInvoice->editRow($invoice_data, null, $orders_data);

                return response()->json(['success' => true, 200]);
            }
        }
    }

    /**
     * Gedt data for populating edit fields
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->only('invoice_id');

            $row = $this->mdlInvoice->getInvoiceById($id);
            $status = $row->first()->status;
            return response()->json(['success' => true, 'status' => $status], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->ajax()) {
            $currentUrl = $request->input('currentUrl');
            $id = $request->input('id');
            // get values from filters
            $search = $request->input('search');
            $company_search = $request->input('company');
            // get current page number from which delete was executed
            $currentUrl = str_replace("/delete", "", $currentUrl);

            $this->mdlInvoice->deleteRow($id);
            // preserve search filters if there is value
            if (!empty($search) || !empty($company_search)) {
                $paginator_data = $this->mdlInvoice->filterInvoices($search, $company_search);
                return response()->json([
                    'success' => true,
                    'paginatorData' => $paginator_data,
                    'currentUrl' => $currentUrl,
                    'filter' => '?search=' . $search . '&company=' . $company_search . '&',
                ], 200);
            } else {
                $paginator_data = $this->mdlInvoice->read();
                return response()->json(['success' => true, 'paginatorData' => $paginator_data, 'currentUrl' => $currentUrl], 200);
            }

        }
    }

    public function printInvoices($invoice_id)
    {
        $invoice_data = $this->mdlInvoice->getInvoiceById($invoice_id);
        $orders_data = $this->mdlInvoice->getOrderByInvoiceId($invoice_id);
        // return view('main/invoices/invoices_print', [
        //     'invoice_data' => $invoice_data,
        //     'orders_data' => $orders_data,
        // ]);

        $pdf = PDF::loadView('main.invoices.invoices_print', [
            'invoice_data' => $invoice_data,
            'orders_data' => $orders_data,
        ]);
        return $pdf->stream($invoice_data->name);
        return view('main/invoices/invoices_print');
        //return $pdf->download('customers.pdf');

    }
}
