<?php

namespace App\Http\Controllers\Main;

use App\Company;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Validator;

class ProductsController extends Controller
{

    private $mdlProduct;
    private $mdlCompany;
    private $pageCustomJs;
    private $pageVendorJs;
    private $pageVendorCss;
    /**
     *  Load page specific and custom made js and css files
     */
    public function __construct()
    {
        $this->mdlProduct = new Product;
        $this->mdlCompany = new Company;
        $this->pageCustomJs = [
            'js/products.js',
        ];
        $this->pageVendorJs = [
            'assets/vendor/pnotify/pnotify.custom.js',
            'assets/vendor/bootstrap-confirmation/bootstrap-confirmation.js',
            'assets/vendor/select2/js/select2.js',

        ];
        $this->pageVendorCss = [
            'assets/vendor/pnotify/pnotify.custom.css',
            'assets/vendor/select2/css/select2.css',
        ];
    }

    public function index(Request $request)
    {
        $company_data = $this->mdlCompany->getAllRows();
        if ($request->has('search') || $request->has('company')) {
            $params = $request->input('search');
            $company_search = $request->input('company');
            $data = $this->mdlProduct->filterProducts($params, $company_search);

            return view('main/products', ['data' => $data,
                'search_data' => $params,
                'company_search' => $company_search,
                'pageCustomJs' => $this->pageCustomJs,
                'pageVendorJs' => $this->pageVendorJs,
                'pageVendorCss' => $this->pageVendorCss,
                'company_data' => $company_data,
            ]);
        } else {
            $data = $this->mdlProduct->read();

            return view('main/products', ['data' => $data,
                'pageCustomJs' => $this->pageCustomJs,
                'pageVendorJs' => $this->pageVendorJs,
                'pageVendorCss' => $this->pageVendorCss,
                'company_data' => $company_data,
            ]);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            Validator::make($request->input('formData'), [
                'name' => 'required|unique:products',
                'price' => 'required|numeric',
                'trade_discount' => 'numeric',
                'vat' => 'numeric',
            ])->validate();

            $params = collect($request->input('formData'))->except("_token", "id");
            $search = $request->input('search');
            $company_search = $request->input('company');
            $this->mdlProduct->insertRow($params);
            // preserve search filter if there is any value
            if (!empty($search) || !empty($company_search)) {
                $paginator_data = $this->mdlProduct->filterProducts($search, $company_search);
                return response()->json([
                    'success' => true, 
                    'paginatorData' => $paginator_data, 
                    'filter' => '?search='.$search.'&company='.$company_search.'&'
                ], 200);
            } else {
                $paginator_data = $this->mdlProduct->read();
                return response()->json(['success' => true, 'paginatorData' => $paginator_data], 200);
            }

            
        }
    }

    /**
     * Update data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function put(Request $request)
    {
        if ($request->ajax()) {
            $params = $request->except('_token');

            /* -------------------------------------------
             * in order to ignore existing id for validation
             * proper format is:
             * unique:table,column,except,idColumn
             */
            $request->validate([
                'name' => 'required|unique:products,name,' . $params['id'],
                'price' => 'required|numeric',
                'trade_discount' => 'numeric',
                'vat' => 'numeric',
            ]);

            $this->mdlProduct->editRow($params);

            return response()->json(['success' => true, 200]);
        }
    }

    /**
     * Gedt data for populating edit fields
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->only('id');

            $row = $this->mdlProduct->getProductById($id);
            return response()->json(['success' => true, 'data' => $row], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->ajax()) {
            $currentUrl = $request->input('currentUrl');
            $id = $request->input('id');
            // get values from filters
            $search = $request->input('search');
            $company_search = $request->input('company');
            // get current page number from which delete was executed
            $currentUrl = str_replace("/delete", "", $currentUrl);

            $this->mdlProduct->deleteRow($id);
            // preserve search filters if there is value
            if (!empty($search) || !empty($company_search)) {
                $paginator_data = $this->mdlProduct->filterProducts($search, $company_search);
                return response()->json([
                    'success' => true, 
                    'paginatorData' => $paginator_data, 
                    'currentUrl' => $currentUrl,
                    'filter' => '?search='.$search.'&company='.$company_search.'&'
                ], 200);
            } else {
                $paginator_data = $this->mdlProduct->read();
                return response()->json(['success' => true, 'paginatorData' => $paginator_data, 'currentUrl' => $currentUrl], 200);
            }
            
            
        }
    }

}
