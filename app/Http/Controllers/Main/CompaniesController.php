<?php

namespace App\Http\Controllers\Main;

use App\Company;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Image;
use Validator;

class CompaniesController extends Controller
{

    private $mdlCompany;
    private $pageCustomJs;
    private $pageVendorJs;
    private $pageVendorCss;
    /**
     *  Load page specific and custom made js and css files
     */
    public function __construct()
    {
        $this->mdlCompany = new Company;
        $this->pageCustomJs = [
            'js/companies.js',
        ];
        $this->pageVendorJs = [
            'assets/vendor/pnotify/pnotify.custom.js',
            'assets/vendor/bootstrap-confirmation/bootstrap-confirmation.js',
            'assets/vendor/dropzone/dropzone.js',

        ];
        $this->pageVendorCss = [
            'assets/vendor/pnotify/pnotify.custom.css',
            'assets/vendor/dropzone/basic.css',
            'assets/vendor/dropzone/dropzone.css',
        ];
    }

    public function index(Request $request)
    {
        if ($request->has('search')) {
            $params = $request->input('search');
            $data = $this->mdlCompany->filterCompanies($params);
            return view('main/companies', ['data' => $data,
                'search_data' => $params,
                'pageCustomJs' => $this->pageCustomJs,
                'pageVendorJs' => $this->pageVendorJs,
                'pageVendorCss' => $this->pageVendorCss,
            ]);
        } else {
            $data = $this->mdlCompany->read();

            return view('main/companies', ['data' => $data,
                'pageCustomJs' => $this->pageCustomJs,
                'pageVendorJs' => $this->pageVendorJs,
                'pageVendorCss' => $this->pageVendorCss,
            ]);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            Validator::make($request->input('formData'), [
                'name' => 'required|unique:companies',
                'city' => 'required',
                'address' => 'required',
                'phone' => 'required|numeric',
                'TIN' => 'required|numeric',
                'identifier' => 'nullable|numeric',
                'bank_account' => 'nullable|regex:[^[0-9]{3}[-]{1}[0-9]{1,13}[-]{1}[0-9]{2}$]',
            ])->validate();

            $params = collect($request->input('formData'))->except("_token", "id");
            $search = $request->input('search');
            $this->mdlCompany->insertRow($params);
            // preserve search filter if there is any value
            if (!empty($search)) {
                $paginator_data = $this->mdlCompany->filterCompanies($search);
                return response()->json([
                    'success' => true,
                    'paginatorData' => $paginator_data,
                    'filter' => '?search=' . $search . '&',
                ], 200);
            } else {
                $paginator_data = $this->mdlCompany->read();
                return response()->json(['success' => true, 'paginatorData' => $paginator_data], 200);
            }

        }
    }

    /**
     * Update data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function put(Request $request)
    {
        if ($request->ajax()) {
            $params = $request->except('_token');

            /* -------------------------------------------
             * in order to ignore existing id for validation
             * proper format is:
             * unique:table,column,except,idColumn
             */
            $request->validate([
                'name' => 'required|unique:companies,name,' . $params['id'],
                'city' => 'required',
                'address' => 'required',
                'phone' => 'required|numeric',
                'TIN' => 'required|numeric',
                'identifier' => 'nullable|numeric',
                'bank_account' => 'nullable|regex:[^[0-9]{3}[-]{1}[0-9]{1,13}[-]{1}[0-9]{2}$]',
            ]);
            $file_name = $this->mdlCompany->getCompanyById($params['id'])->logo; //die(var_dump($file_name, $params['logo']));
            if ($params['logo'] === null) {
                $params = collect($params)->except('logo');
                $this->mdlCompany->editRow($params);
            } elseif ($file_name === null) {
                $this->mdlCompany->editRow($params);
            } else {
                Storage::delete('public/' . $file_name);
                $this->mdlCompany->editRow($params);
            }
            return response()->json(['success' => true], 200);
        }
    }

    /**
     * Gedt data for populating edit fields
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->only('id');

            $row = $this->mdlCompany->getCompanyById($id);
            return response()->json(['success' => true, 'data' => $row], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->ajax()) {
            $currentUrl = $request->input('currentUrl');
            $id = $request->input('id');
            $search = $request->input('search');
            // get current page number from which delete was executed
            $currentUrl = str_replace("/delete", "", $currentUrl);
            $file_name = $this->mdlCompany->getCompanyById($id)->logo;
            if ($file_name !== null) {
                Storage::delete('public/' . $file_name);
            }
            $this->mdlCompany->deleteRow($id);
            // preserve search filters if there is value
            if (!empty($search)) {
                $paginator_data = $this->mdlCompany->filterCompanies($search);
                return response()->json([
                    'success' => true,
                    'paginatorData' => $paginator_data,
                    'currentUrl' => $currentUrl,
                    'filter' => '?search='.$search.'&',
                ], 200);
            } else {
                $paginator_data = $this->mdlCompany->read();
                return response()->json([
                    'success' => true,
                    'paginatorData' => $paginator_data,
                    'currentUrl' => $currentUrl,
                ], 200);
            }

        }
    }
    /**
     * Upload logo to storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function uploadLogo(Request $request)
    {
        if ($request->ajax()) {
            Validator::make($request->all(), [
                'file' => 'mimes:jpg,jpeg,bmp,png',

            ])->validate();

            if ($request->method() === "GET") {
                if ($request->input('delete_file')) {
                    $file_name = $request->input('file_name');

                    Storage::delete('public/' . $file_name);
                    return response()->json(['success' => true, 200]);
                }
            } elseif ($request->method() === "POST") {
                $file = $request->file('file');
                $file_name = 'company_logo_' . randomStringGenerator(6) . '.' . $file->getClientOriginalExtension();
                $destination = 'public';
                //$file->storePubliclyAs($destination, $file_name);

                Image::make($file->getRealPath())->crop(50, 50)->save(storage_path('app/public/') . $file_name);

                return response()->json(['success' => true, 'file_name' => $file_name, 200]);
            }

        }
    }
}
