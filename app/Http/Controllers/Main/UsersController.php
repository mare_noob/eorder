<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\User;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Image;
use Validator;

class UsersController extends Controller
{

    private $mdlUser;
    private $mdlCompany;
    private $pageCustomJs;
    private $pageVendorJs;
    private $pageVendorCss;
    /**
     *  Load page specific and custom made js and css files
     */
    public function __construct()
    {
        $this->mdlUser = new User;
        $this->mdlCompany = new Company;
        $this->pageCustomJs = [
            'js/users.js',
        ];
        $this->pageVendorJs = [
            'assets/vendor/pnotify/pnotify.custom.js',
            'assets/vendor/bootstrap-confirmation/bootstrap-confirmation.js',
            'assets/vendor/dropzone/dropzone.js',
            'assets/vendor/select2/js/select2.js',
        ];
        $this->pageVendorCss = [
            'assets/vendor/pnotify/pnotify.custom.css',
            'assets/vendor/dropzone/basic.css',
            'assets/vendor/dropzone/dropzone.css',
            'assets/vendor/select2/css/select2.css',
        ];
    }

    public function index(Request $request)
    {
        $company_data = $this->mdlCompany->getAllRows();
        if ($request->has('search')) {
            $params = $request->input('search');
            $data = $this->mdlUser->filterUsers($params);
            return view('main/users', [
                'data' => $data,
                'company_data' => $company_data,
                'search_data' => $params,
                'pageCustomJs' => $this->pageCustomJs,
                'pageVendorJs' => $this->pageVendorJs,
                'pageVendorCss' => $this->pageVendorCss,
            ]);
        } else {
            $data = $this->mdlUser->read();

            return view('main/users', [
                'data' => $data,
                'company_data' => $company_data,
                'pageCustomJs' => $this->pageCustomJs,
                'pageVendorJs' => $this->pageVendorJs,
                'pageVendorCss' => $this->pageVendorCss,
            ]);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->ajax()) {
            Validator::make($request->input('formData'), [
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required|email|unique:users',
                'username' => 'required|unique:users',
                'password' => 'required|min:5|same:password_confirmation',
                'password_confirmation' => 'same:password',
            ])->validate();

            $params = collect($request->input('formData'))->except("_token", "password_confirmation", "id");
            $search = $request->input('search');
            $this->mdlUser->insertRow($params);
            // preserve search filter if there is any value
            if (!empty($search)) {
                $paginator_data = $this->mdlUser->filterUsers($search);
                return response()->json([
                    'success' => true,
                    'paginatorData' => $paginator_data,
                    'filter' => '?search=' . $search . '&',
                ], 200);
            } else {
                $paginator_data = $this->mdlUser->read();
                return response()->json(['success' => true, 'paginatorData' => $paginator_data], 200);
            }

        }
    }

    /**
     * Update data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function put(Request $request)
    {
        if ($request->ajax()) {
            $params = $request->except('_token', 'password_confirmation');

            /* -------------------------------------------
             * in order to ignore existing id for validation
             * proper format is:
             * unique:table,column,except,idColumn
             */
            $request->validate([
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required|email|unique:users,email,' . $params['id'],
                'username' => 'required|unique:users,username,' . $params['id'],
                'password' => 'nullable|min:5|confirmed',
            ]);
            // get file name from db
            $file_name = $this->mdlUser->getUserById($params['id'])->image; //die(var_dump($file_name, $params['image']));
            // if there is no image from put request save parameters in db without file name
            
            if ($params['image'] === null) {
                $params = collect($params)->except('image');
                $this->mdlUser->editRow($params);
                // if there is no file name in db insert all parameters including file name
            } elseif ($file_name === null) {
                $this->mdlUser->editRow($params);
                // if there is file from request and there is file in db overwrite it and delete old image
            } else {
                Storage::delete('public/' . $file_name);
                $this->mdlUser->editRow($params);
            }

            return response()->json(['success' => true], 200);
        }
    }

    /**
     * Gedt data for populating edit fields
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->only('id');

            $row = $this->mdlUser->getUserById($id);
            return response()->json(['success' => true, 'data' => $row], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->ajax()) {
            $currentUrl = $request->input('currentUrl');
            $id = $request->input('id');
            $search = $request->input('search');
            // get current page number from which delete was executed
            $currentUrl = str_replace("/delete", "", $currentUrl);
            // get file name
            $file_name = $this->mdlUser->getUserById($id)->image;
            // if there is file name stored in db delete it
            if ($file_name !== null) {
                Storage::delete('public/' . $file_name);
            }

            $this->mdlUser->deleteRow($id);
            // preserve search filters if there is value
            if (!empty($search)) {
                $paginator_data = $this->mdlUser->filterUsers($search);
                return response()->json([
                    'success' => true,
                    'paginatorData' => $paginator_data,
                    'currentUrl' => $currentUrl,
                    'filter' => '?search=' . $search . '&',
                ], 200);

            } else {
                $paginator_data = $this->mdlUser->read();
                return response()->json([
                    'success' => true,
                    'paginatorData' => $paginator_data,
                    'currentUrl' => $currentUrl
                ], 200);
            }

        }
    }
    /**
     * Upload image to storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(Request $request)
    {
        if ($request->ajax()) {
            Validator::make($request->all(), [
                'file' => 'mimes:jpg,jpeg,bmp,png',

            ])->validate();

            if ($request->method() === "GET") {
                if ($request->input('delete_file')) {
                    $file_name = $request->input('file_name');

                    Storage::delete('public/' . $file_name);
                    return response()->json(['success' => true], 200);
                }
            } elseif ($request->method() === "POST") {
                $file = $request->file('file');
                $file_name = 'user_image_' . randomStringGenerator(6) . '.' . $file->getClientOriginalExtension();
                //$destination = 'public';
                //$file->storePubliclyAs($destination, $file_name);

                Image::make($file->getRealPath())->crop(50, 50)->save(storage_path('app/public/') . $file_name);

                return response()->json(['success' => true, 'file_name' => $file_name], 200);
            }
        }
    }
}
