<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;
    /**
     *
     * Show all data from table with pagination
     *
     * @param    int  $per_page paginator parameter how much results to display
     * @return   eloquent object
     *
     */
    public function read($per_page = 15)
    {
        $join = Invoice::Leftjoin(
            'users',
            'users.id', '=', 'invoices.user_id')
            ->Leftjoin('companies as companies1',
                'companies1.id', '=', 'invoices.by_company_id')
            ->Leftjoin('companies as companies2',
                'companies2.id', '=', 'invoices.for_company_id')->
            select('invoices.*', 'users.fname as user_fname', 'users.lname as user_lname', 'companies1.name as by_company_name', 'companies2.name as for_company_name')->orderBy('id', 'desc');
        $result = $join->paginate($per_page);
        return $result;
    }
    /**
     *
     * Show all data from table
     * @return   eloquent object
     *
     */
    public function getAllRows()
    {

        $result = Invoice::withTrashed()->get();
        return $result;
    }

    /**
     *
     * Inserts data into table
     *
     * @param    array  $params data from html form
     * @return   void
     *
     */
    public function insertRow($params, $orders_data)
    {
        $invoice = new Invoice;

        foreach ($params as $key => $value) {
            if ($key == 'signed' && $value == 'true') {
                $invoice->status = 'paid';
                $invoice->$key = $value;
            } else {
                $invoice->$key = $value;
            }

        }
        $invoice->save();
        $invoice_id = $invoice->id;
        foreach ($orders_data as $key => $value) {
            $order = new Order;
            foreach ($value as $subkey => $item) {

                $order->$subkey = $item;
            }
            $order->invoice_id = $invoice_id;
            $order->save();

        }

    }
    /**
     *
     * Edit data in table
     *
     * @param    array  $params data from html form
     * @return   void
     *
     */
    public function editRow($invoice_data, $type = null, $orders_data = null)
    {
        if ($type === null) {
            $invoice = Invoice::find($invoice_data['invoice_id']);
            $invoice_data = collect($invoice_data)->except('invoice_id');
            foreach ($invoice_data as $key => $value) {
                $invoice->$key = $value;
            }
            $invoice->save();
            $invoice_id = $invoice->id;
            Order::where('invoice_id', '=', $invoice_id)->delete();
            Order::where('invoice_id', '=', $invoice_id)->forceDelete();
            foreach ($orders_data as $key => $value) {
                $order = new Order;
                foreach ($value as $subkey => $item) {

                    $order->$subkey = $item;
                }
                $order->invoice_id = $invoice_id;
                $order->save();

            }
        } else {
            $invoice = Invoice::find($invoice_data['invoice_id']);
            $invoice->status = $invoice_data['status'];
            $invoice->save();
        }

    }
    /**
     *
     * Delete data from table
     *
     * @param    int  $id primary key in table
     * @return   void
     *
     */
    public function deleteRow($id)
    {
        $orders = Order::where('invoice_id', '=', $id)->get();
        if (!$orders->isEmpty()) {
            Order::where('invoice_id', '=', $id)->delete();
        }
        Invoice::find($id)->delete();
    }
    /**
     *
     * Searches data from table
     *
     * @param    string  $param string to search data in table
     * @param    string  $param string to search data in table
     * @return   eloquent object
     *
     */
    public function filterInvoices($param, $company_id, $per_page = 15)
    {

        $join = Invoice::Leftjoin(
            'users',
            'users.id', '=', 'invoices.user_id')
            ->Leftjoin('companies as companies1',
                'companies1.id', '=', 'invoices.by_company_id')
            ->Leftjoin('companies as companies2',
                'companies2.id', '=', 'invoices.for_company_id')->
            select('invoices.*', 'users.fname as user_fname', 'users.lname as user_lname', 'companies1.name as by_company_name', 'companies2.name as for_company_name')->where('invoices.for_company_id', '=', $company_id);
        $rows = $join->paginate($per_page);
        //$rows = Invoice::where('name', 'like', '%' . $param . '%')->orWhere('address', 'like', '%' . $param . '%')->orWhere('phone', 'like', '%' . $param . '%')->paginate($per_page);

        return $rows;
    }
    //
    public function calculateTotal($company_id)
    {
        if ($company_id === 'all') {
            $debt_cash = Invoice::whereIn('status', ['sent', 'processing'])->where('type', '=', 'cash')->sum('total');
            $debt_bill = Invoice::whereIn('status', ['sent', 'processing'])->where('type', '=', 'bill')->sum('total');
            $paid_cash = Invoice::where('status', '=', 'paid')->where('type', '=', 'cash')->sum('total');
            $paid_bill = Invoice::where('status', '=', 'paid')->where('type', '=', 'bill')->sum('total');
        } else {
            $debt_cash = Invoice::where('for_company_id', '=', $company_id)->whereIn('status', ['sent', 'processing'])->where('type', '=', 'cash')->sum('total');
            $debt_bill = Invoice::where('for_company_id', '=', $company_id)->whereIn('status', ['sent', 'processing'])->where('type', '=', 'bill')->sum('total');
            $paid_cash = Invoice::where('for_company_id', '=', $company_id)->where('status', '=', 'paid')->where('type', '=', 'cash')->sum('total');
            $paid_bill = Invoice::where('for_company_id', '=', $company_id)->where('status', '=', 'paid')->where('type', '=', 'bill')->sum('total');
        }

        return ['debt_cash' => $debt_cash, 'debt_bill' => $debt_bill, 'paid_cash' => $paid_cash, 'paid_bill' => $paid_bill];
    }
    /**
     *
     * Get reult by id
     *
     * @param    int  $id primary key in table
     * @return   eloquent object
     *
     */
    public function getInvoiceById($id)
    {
        //$single_row = Invoice::find($id);

        $join = Invoice::Leftjoin(
            'users',
            'users.id', '=', 'invoices.user_id')
            ->Leftjoin('companies as companies1',
                'companies1.id', '=', 'invoices.by_company_id')
            ->Leftjoin('companies as companies2',
                'companies2.id', '=', 'invoices.for_company_id')->
            select('invoices.*', 'users.fname as user_fname', 'users.lname as user_lname', 'users.admin_role as user_role', 'companies1.name as by_company_name', 'companies1.city as by_company_city', 'companies1.address as by_company_address', 'companies1.phone as by_company_phone', 'companies1.bank_account as by_company_bank_account', 'companies1.TIN as by_company_TIN', 'companies2.name as for_company_name', 'companies2.city as for_company_city', 'companies2.address as for_company_address', 'companies2.phone as for_company_phone', 'companies2.TIN as for_company_TIN', 'companies2.vat_system as for_company_vat_system');

        $result = $join->find($id);
        return $result;
    }

    public function getOrderByInvoiceId($id)
    {
        $orders = Order::join('products', 'products.id', '=', 'orders.product_id')
            ->where('orders.invoice_id', '=', $id)
            ->select('orders.*', 'products.name as product_name')->get();
        return $orders;
    }
}
