<?php

namespace App;

use App\ProductsToCompany;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;
    private $mdlProductsToCompany;

    public function __construct()
    {
        $this->mdlProductsToCompany = new ProductsToCompany;
    }
    /**
     *
     * Show all data from table with pagination
     *
     * @param    int  $per_page paginator parameter how much results to display
     * @return   eloquent object
     *
     */
    public function read($per_page = 15)
    {

        //$result = Product::paginate($per_page);

        $result = ProductsToCompany::Leftjoin('companies', 'products_to_companies.company_id', '=', 'companies.id')
            ->Rightjoin('products', 'products.id', '=', 'products_to_companies.product_id')
            ->select('products.*', 'companies.name AS company_name', 'products_to_companies.company_id AS company_id')->paginate($per_page);
        return $result;
    }

    /**
     *
     * Inserts data into table
     *
     * @param    array  $params data from html form
     * @return   void
     *
     */
    public function insertRow($params)
    {
        $product = new Product;
        foreach ($params as $key => $value) {
            $product->$key = $value;
        }
        $total = $product->price;
        if ($product->vat !== 0) {
            $total = $total + calculateVat($total, $product->vat);
        }
        if ($product->trade_discount !== 0) {
            $total = $total - calculateTradeDiscount($total, $product->trade_discount);
        }
        $product->total = $total;
        $product->save();
        $product_id = $product->id;

        $this->mdlProductsToCompany->product_id = $product_id;
        $this->mdlProductsToCompany->company_id = $params['company_id'];
        $this->mdlProductsToCompany->save();

    }
    /**
     *
     * Edit data in table
     *
     * @param    array  $params data from html form
     * @return   void
     *
     */
    public function editRow($params)
    {
        $product = Product::find($params['id']);
        foreach ($params as $key => $value) {
            $product->$key = $value;
        }
        $total = $product->price;
        if ($product->vat !== 0) {
            $total = $total + calculateVat($total, $product->vat);
        }
        if ($product->trade_discount !== 0) {
            $total = $total - calculateTradeDiscount($total, $product->trade_discount);
        }
        $product->total = $total;
        $product->save();

        $products_to_companies = $this->mdlProductsToCompany::where('product_id', $params['id'])->first();
        $products_to_companies->product_id = $params['id'];
        $products_to_companies->company_id = $params['company_id'];
        $products_to_companies->save();

    }
    /**
     *
     * Delete data from table
     *
     * @param    int  $id primary key in table
     * @return   void
     *
     */
    public function deleteRow($id)
    {
        ProductsToCompany::where('product_id', $id)->first()->delete();
        Product::find($id)->delete();
    }
    /**
     *
     * Searches data from table
     *
     * @param    string  $param string to search data in table
     * @param    string  $param string to search data in table
     * @return   eloquent object
     *
     */
    public function filterProducts($param, $company_id, $per_page = 15)
    {
        if ($company_id !== null) {
            $rows = ProductsToCompany::Leftjoin('companies', 'products_to_companies.company_id', '=', 'companies.id')
                ->Rightjoin('products', 'products.id', '=', 'products_to_companies.product_id')
                ->select('products.*', 'companies.name AS company_name', 'products_to_companies.company_id AS company_id')->where([['products.name', 'like', '%' . $param . '%'], ['products.company_id', '=', $company_id]])->paginate($per_page);
        } else {
            $rows = ProductsToCompany::Leftjoin('companies', 'products_to_companies.company_id', '=', 'companies.id')
                ->Rightjoin('products', 'products.id', '=', 'products_to_companies.product_id')
                ->select('products.*', 'companies.name AS company_name', 'products_to_companies.company_id AS company_id')->where([['products.name', 'like', '%' . $param . '%']])->paginate($per_page);
        }

        return $rows;
    }

    public function getProductById($id)
    {
        return Product::find($id);
    }

    public function getProductByCompanyId($company_id)
    {
        return Product::where('company_id', '=', $company_id)->get();
    }
}
