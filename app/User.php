<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{

    use Notifiable;
    // const UPDATED_AT = null;
    // const CREATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     *
     * Show all data from table with pagination
     *
     * @param    int  $per_page paginator parameter how much results to display
     * @return   eloquent object
     *
     */
    public function read($per_page = 15)
    {

        //$result = User::where('admin_role', '>', 0)->paginate($per_page);
        $result = DB::table('users')->Leftjoin(
            'companies', 'companies.id', '=', 'users.company_id'
        )->select(
            'users.*', 'companies.name as company_name'
        )->where('users.admin_role', '>', 0)->paginate($per_page);
        return $result;
    }

    /**
     *
     * Inserts data into table
     *
     * @param    array  $params data from html form
     * @return   void
     *
     */
    public function insertRow($params)
    {
        $user = new User;
        foreach ($params as $key => $value) {
            if ($key == 'password') {
                $user->$key = Hash::make($value);
            } else {
                $user->$key = $value;
            }
        }
        $user->save();
    }
    /**
     *
     * Edit data in table
     *
     * @param    array  $params data from html form
     * @return   void
     *
     */
    public function editRow($params)
    {
        $user = User::find($params['id']);
        foreach ($params as $key => $value) {
            if ($key == 'password') {
                if (!empty($value)) {
                    $user->$key = Hash::make($value);
                } else {
                    continue;
                }

            } else {
                $user->$key = $value;
            }
        }
        $user->save();
    }
    /**
     *
     * Delete data from table
     *
     * @param    int  $id primary key in table
     * @return   void
     *
     */
    public function deleteRow($id)
    {
        User::find($id)->delete();
    }
    /**
     *
     * Searches data from table
     *
     * @param    string  $param string to search data in table
     * @param    string  $param string to search data in table
     * @return   eloquent object
     *
     */
    public function filterUsers($param, $per_page = 15)
    {
        // $rows = User::where('admin_role', '>', 0)->where('fname', 'like', '%' . $param . '%')->orWhere('lname', 'like', '%' . $param . '%')->orWhere('email', 'like', '%' . $param . '%')->paginate($per_page);
        $rows = DB::table('users')->Leftjoin(
            'companies', 'companies.id', '=', 'users.company_id'
        )->select(
            'users.*', 'companies.name as company_name'
        );
        $rows = $rows->where('admin_role', '>', 0)->where(function ($query) use ($param) {
            $query->where('fname', 'like', '%' . $param . '%')->orWhere('lname', 'like', '%' . $param . '%')->orWhere('email', 'like', '%' . $param . '%');
        })->paginate($per_page);
        return $rows;
    }

    public function getUserById($id)
    {
        $result = DB::table('users')->Leftjoin(
            'companies', 'companies.id', '=', 'users.company_id'
        )->select(
            'users.*', 'companies.name as company_name', 'companies.id as company_id'
        )->where('users.id', '=', $id)->first();
        return $result;
    }

}
