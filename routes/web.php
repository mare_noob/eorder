<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    if (Auth::check()) {
        return Redirect::to('index');
    } else {
        return Redirect::to('auth/login');
    }
});

Auth::routes();

/**
 * If something important needs to be done on server, prevent everyone except super admin to enter app
 */
// Route::get('under_construction', function () {
//     return view('under_construction');
// })->name('under_construction');

Route::get('auth/login', 'Auth\LoginController@index')->name('login');

Route::post('auth/login', 'Auth\LoginController@login');

Route::group(['middleware' => ['auth']], function () {

    Route::get('auth/logout', function () {
        auth()->logout();
        return Redirect::to('auth/login');
    });

    Route::get("index", function () {
        return view('index');
    })->name('index');

    Route::prefix('main')->group(function () {
        Route::get('unauthorized', function () {
            return view('unauthorized');
        });
        // users
        Route::get('users', 'Main\UsersController@index')->name('usersIndex');
        //Route::get('users/search', 'Main\UsersController@searchUsers')->name('searchUsers');
        Route::post('users/create', 'Main\UsersController@create')->name('addUserAjax');
        Route::delete('users/delete', 'Main\UsersController@destroy')->name('deleteUserAjax');
        Route::put('users/edit', 'Main\UsersController@put')->name('putUserAjax');
        Route::get('users/getData', 'Main\UsersController@getData')->name('getDataUsers');
        Route::post('users/upload', 'Main\UsersController@uploadImage')->name('uploadImage');
        Route::get('users/upload', 'Main\UsersController@uploadImage')->name('deleteImage');

        // comapnies
        Route::get('companies', 'Main\CompaniesController@index')->name('companiesIndex');
        Route::post('companies/create', 'Main\CompaniesController@create')->name('addCompanyAjax');
        Route::delete('companies/delete', 'Main\CompaniesController@destroy')->name('deleteCompanyAjax');
        Route::put('companies/edit', 'Main\CompaniesController@put')->name('putCompanyAjax');
        Route::get('companies/getData', 'Main\CompaniesController@getData')->name('getDataCompanies');
        Route::post('companies/upload', 'Main\CompaniesController@uploadLogo')->name('uploadLogo');
        Route::get('companies/upload', 'Main\CompaniesController@uploadLogo')->name('deleteLogo');

        // products
        Route::get('products', 'Main\ProductsController@index')->name('productsIndex');
        Route::post('products/create', 'Main\ProductsController@create')->name('addProductAjax');
        Route::delete('products/delete', 'Main\ProductsController@destroy')->name('deleteProductAjax');
        Route::put('products/edit', 'Main\ProductsController@put')->name('putProductAjax');
        Route::get('products/getData', 'Main\ProductsController@getData')->name('getDataCompanies');

        // invoices
        Route::prefix('invoices')->group(function () {
            Route::get('invoices_index', 'Main\InvoicesController@index')->name('invoicesIndex');
            Route::get('invoices_create/invoice_id/{invoice_id?}', 'Main\InvoicesController@showCreateInvoices')->name('create_invoices_page');
            Route::post('invoices_create', 'Main\InvoicesController@createInvoices')->name('createInvoice');
            Route::delete('invoices_index/delete', 'Main\InvoicesController@destroy')->name('deleteInvoiceAjax');
            Route::put('invoices_index/edit', 'Main\InvoicesController@put')->name('editInvoiceStatus');
            Route::put('invoices_create/edit', 'Main\InvoicesController@put')->name('editInvoice');
            Route::get('invoices_view/invoice_id/{invoice_id}', 'Main\InvoicesController@viewInvoices')->name('viewInvoices');
            Route::get('invoices_print/invoice_id/{invoice_id}', 'Main\InvoicesController@printInvoices')->name('printInvoices');
            Route::get('invoices_index/getData', 'Main\InvoicesController@getData')->name('getDataInvoices');
        });
    });
});
