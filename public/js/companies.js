$(document).ready(function () {



    // clear fields
    function clearFields() {
        $('#cid').val('');
        $('#name').val('');
        $('#city').val('');
        $('#address').val('');
        $('#phone').val('');
        $('#TIN').val('');
        $('#identifier').val('');
        $('#bank_account').val('');
        $('#vat_system,select>option:eq(0)').prop('selected', true);
        // remove image from drop field and delete file
        if (document.getElementsByClassName('dz-remove')[0]) {
            document.getElementsByClassName('dz-remove')[0].click();
        }

    }
    // activate add modal
    $('.companyModal').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name',
        modal: true,
        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
            beforeOpen: function () {
                if ($(window).width() < 700) {
                    this.st.focus = false;
                } else {
                    this.st.focus = '#name';
                }
            }
        }
    }).click(function () {
        $('#companyModal').find('h2.panel-title').text('Dodaj kompaniju');
        $('.modal-confirm').attr('data-type', 'create');
        clearFields();
    });
    // activate edit modal and populate fields
    $('.edit').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name',
        modal: true,
        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
            beforeOpen: function () {
                if ($(window).width() < 700) {
                    this.st.focus = false;
                } else {
                    this.st.focus = '#name';
                }
            }
        }
    }).click(function () {
        $('#companyModal').find('h2.panel-title').text('Edituj kompaniju');
        $('.modal-confirm').attr('data-type', 'edit');
        var a = $(this).attr('data-number');
        // get data from db and populate fields
        $.ajax({
            url: URL + "/main/companies/getData",
            data: { id: a },
            type: "GET",
            dataType: "json",
            success: function (response) {
                if (response.success == true) {

                    var ajaxData = response.data[0];

                    $('#cid').val(ajaxData.id);
                    $('#name').val(ajaxData.name);
                    $('#city').val(ajaxData.city);
                    $('#address').val(ajaxData.address);
                    $('#phone').val(ajaxData.phone);
                    $('#TIN').val(ajaxData.TIN);
                    $('#identifier').val(ajaxData.identifier);
                    $('#bank_account').val(ajaxData.bank_account);
                    $('#vat_system').val(ajaxData.vat_system);
                    $('#file').val('');
                    $('.modal-confirm').attr('data-type', 'edit');
                }
            },
            error: function () {
                console.log('greska');
            }

        });
    });
    /*
     Modal Dismiss
     */
    $('.modal-dismiss').click(function (e) {
        e.preventDefault();
        $('.errors').remove();
        $('.input_error').removeClass('input_error');
        $('.modal-confirm').attr('data-type', 'create');
        clearFields();
        $.magnificPopup.close();
    });
    /*
     Modal Confirm
     */
    // create company

    $('.modal-confirm').click(function (e) {
        e.preventDefault();
        var formData = formDataToArray($("#formData").serializeArray());
        // validation fields and rulles
        var fields_to_check = {
            'name': {
                'required': '',
            },
            'city': {
                'required': '',
            },
            'address': {
                'required': '',
            },
            'phone': {
                'numeric': '',
                'required': '',
            },
            'TIN': {
                'numeric': '',
                'required': '',
            },
            'identifier': {
                'numeric': '',
                'nullable': '',
            },
            'bank_account': {
                'regex': '^[0-9]{3}[-]{1}[0-9]{1,13}[-]{1}[0-9]{2}$',
                'nullable': '',
            },
        }

        if ($('.modal-confirm').attr('data-type') == 'create') {
            // create data
            // js validation
            var is_valid_create = new ValidateFields(formData, fields_to_check);
            is_valid_create.validate();
            if (is_valid_create.evaluate().success) {
                $.ajax({
                    url: URL + "/main/companies/create",
                    data: { formData: formData, search: $('#search').val() },
                    type: "POST",
                    dataType: "json",
                    success: function (response) {
                        if (response.success == true) {
                            notify('Uspeh', 'Kompanija je dodata', 'success');
                            setTimeout(function () {
                                paginate_handling(response.paginatorData, null, response.filter);
                            }, 1000);

                            $.magnificPopup.close();
                        }
                    },
                    error: function (res) {
                        var errors = res.responseJSON.errors;
                        showErrors(errors);
                    }

                });
            } else {
                showErrors(is_valid_create.evaluate().errors);
            }

        } else {
            // edit data
            // js validation
            var is_valid_edit = new ValidateFields(formData, fields_to_check);
            is_valid_edit.validate();
            if (is_valid_edit.evaluate().success) {
                $.ajax({
                    url: URL + '/main/companies/edit',
                    data: $("#formData").serialize(),
                    type: "PUT",
                    dataType: "json",
                    success: function (response) {

                        if (response.success == true) {
                            notify('Uspeh', 'Kompanija je editovana', 'info');
                            setTimeout(function () {
                                location.reload();
                            }, 1000);

                            $.magnificPopup.close();
                        }
                    },
                    error: function (res) {
                        var errors = res.responseJSON.errors;
                        showErrors(errors);
                    }

                });
            } else {
                showErrors(is_valid_edit.evaluate().errors);
            }
        }
    });
    /*
     Bootstrap Confirmation - CALLBACK
     */
    // delete company
    var x = null;
    $('.delete').click(function () {
        x = $(this);
    });
    $('.delete').confirmation({
        placement: 'left',
        onConfirm: function () {
            $.ajax({
                url: URL + '/main/companies/delete',
                data: { currentUrl: window.location.href, id: x.attr('data-number'), search: $('#search').val() },
                type: "DELETE",
                dataType: "json",

                success: function (response) {

                    if (response.success == true) {
                        notify('Uspeh', 'Kompanija je obrisana', 'warning');
                        setTimeout(function () {
                            paginate_handling(response.paginatorData, response.currentUrl, response.filter);
                        }, 1000);

                        $.magnificPopup.close();
                    }
                },
                error: function () {
                    console.log('greska');
                }

            });
        },
        onCancel: function () {

        }
    });
    // search companies
    $('#search').keyup(function (e) {
        if (e.which == 13) {
            window.location.href = URL + '/main/companies?search=' + $(this).val();
        }
    });
    // search on button
    $('#button_search').click(function () {
        window.location.href = URL + '/main/companies?search=' + $('#search').val();
    });

    // dropzone
    Dropzone.autoDiscover = false;
    $('#my_dropzone').dropzone({
        url: URL + "/main/companies/upload",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        //autoProcessQueue: false,
        maxFiles: 1,
        addRemoveLinks: true,
        acceptedFiles: 'image/*',
        dictInvalidFileType: 'invalid data type',
        init: function () {
            this.on("maxfilesexceeded", function (file) { this.removeFile(file); });

        },
        success: function (file, response) {
            var imgName = response.file_name;
            file.previewElement.classList.add("dz-success");
            console.log("Successfully uploaded :" + imgName);
            $('#file').val(imgName);

            $('.dz-remove').on('click', function () {
                $.ajax({

                    url: URL + "/main/companies/upload",
                    data: { delete_file: true, file_name: imgName },
                    type: "GET",
                    dataType: 'json',
                    success: function (response) {
                        $('#file').val('');
                    },
                    error: function (res) {
                        console.log('error');
                    }
                });
            });
        },
        error: function (file, response) {
            file.previewElement.classList.add("dz-error");

            if (response.exception === "Illuminate\\Http\\Exceptions\\PostTooLargeException") {
                $('.dz-error-message').prepend('<p style="width:100%;margin:0 auto" class="text-center">File size too large</p>');
            } else {
                $('.dz-error-message').prepend('<p style="width:100%;margin:0 auto" class="text-center">' + response + '</p>');
            }
            showErrors(response.errors);
            //console.log(response.errors.file[0]);
        }
    }).addClass('dropzone');




});