$(document).ready(function () {
    // initialize select2 plugin for product create
    $('#company_id').select2({
        width: '100%',
        // this fixes serach input problem on select2
        dropdownParent: $("#productModal"),

    });
    // initialize select2 plugin for search filter
    $('#company_search').select2({
        width: '100%',
    });

    // when focus erase 0 if no value return to 0
    $("#trade_discount").focusin(function () {
        if ($(this).val() === '0') {
            $(this).val('');
        }
    });
    $("#trade_discount").focusout(function () {
        if ($(this).val() === '') {
            $(this).val('0');
        }

    });
    // when focus erase 0 if no value return to 0
    $("#vat").focusin(function () {
        if ($(this).val() === "0") {
            $(this).val('');
        }
    });
    $("#vat").focusout(function () {
        if ($(this).val() === '') {
            $(this).val('0');
        }

    });

    // clear fields
    function clearFields() {
        $('#pid').val('');
        $('#name').val('');
        //$("#company_id")[0].selectedIndex = 0;
        // select first element in select2 js plugin
        $('#company_id').val($('#company_id option:first-child').val()).trigger('change');
        $('#price').val('');
        $('#trade_discount').val('0');
        $('#vat').val('0');
    }
    // reset filters
    $('#reset_filters').on('click', function () {
        $('#search').val('');
        $('#company_search').val($('#company_search option:first-child').val()).trigger('change');
        window.location.href = URL + '/main/products';
    });
    // activate add modal
    $('.productModal').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name',
        modal: true,
        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
            beforeOpen: function () {
                if ($(window).width() < 700) {
                    this.st.focus = false;
                } else {
                    this.st.focus = '#name';
                }
            }
        }
    }).click(function () {
        $('#productModal').find('h2.panel-title').text('Dodaj proizvod');
        $('.modal-confirm').attr('data-type', 'create');
        clearFields();
        if ($('#company_search').val() !== '') {
            $('#company_id').val($('#company_search').val()).trigger('change');
        }
    });
    // activate edit modal and populate fields
    $('.edit').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name',
        modal: true,
        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
            beforeOpen: function () {
                if ($(window).width() < 700) {
                    this.st.focus = false;
                } else {
                    this.st.focus = '#name';
                }
            }
        }
    }).click(function () {
        $('#productModal').find('h2.panel-title').text('Edituj proizvod');
        $('.modal-confirm').attr('data-type', 'edit');
        var a = $(this).attr('data-number');
        // get data from db and populate fields
        $.ajax({
            url: URL + "/main/products/getData",
            data: { id: a },
            type: "GET",
            dataType: "json",
            success: function (response) {
                if (response.success == true) {

                    var ajaxData = response.data[0];

                    $('#pid').val(ajaxData.id);
                    $('#name').val(ajaxData.name);
                    // select value in select2 js plugin
                    $('#company_id').val(ajaxData.company_id).trigger('change');
                    $('#price').val(ajaxData.price);
                    $('#trade_discount').val(ajaxData.trade_discount);
                    $('#vat').val(ajaxData.vat);
                    $('.modal-confirm').attr('data-type', 'edit');
                }
            },
            error: function () {
                console.log('greska');
            }

        });
    });
    /*
     Modal Dismiss
     */
    $('.modal-dismiss').click(function (e) {
        e.preventDefault();
        $('.errors').remove();
        $('.input_error').removeClass('input_error');
        $('.modal-confirm').attr('data-type', 'create');
        clearFields();
        $.magnificPopup.close();
    });
    /*
     Modal Confirm
     */
    // create product and edit

    $('.modal-confirm').click(function (e) {
        e.preventDefault();
        var formData = formDataToArray($("#formData").serializeArray());
        // fields to validate
        var fields_to_check = {
            'name': {
                'required': '',
            },
            'price': {
                'numeric': '',
                'required': '',
            },
            'trade_discount': {
                'numeric': '',
                'nullable': '',
            },
            'vat': {
                'numeric': '',
                'nullable': '',
            },

        }

        if ($('.modal-confirm').attr('data-type') == 'create') {
            // create new record
            // javascript validation
            var is_valid_create = new ValidateFields(formData, fields_to_check);
            is_valid_create.validate();
            if (is_valid_create.evaluate().success) {
                $.ajax({
                    url: URL + "/main/products/create",
                    data: {
                        formData: formData,
                        search: $('#search').val(),
                        company: $('#company_search').val()
                    },
                    type: "POST",
                    dataType: "json",
                    success: function (response) {
                        if (response.success == true) {
                            notify('Uspeh', 'Proizvod je dodat', 'success');
                            setTimeout(function () {
                                paginate_handling(response.paginatorData, null, response.filter);
                            }, 1000);

                            $.magnificPopup.close();
                        }
                    },
                    error: function (res) {
                        var errors = res.responseJSON.errors;
                        showErrors(errors);
                    }

                });
            } else {
                showErrors(is_valid_create.evaluate().errors);
            }

        } else {
            // edit record
            // js validation
            var is_valid_edit = new ValidateFields(formData, fields_to_check);
            is_valid_edit.validate();
            if (is_valid_edit.evaluate().success) {
                $.ajax({
                    url: URL + '/main/products/edit',
                    data: $("#formData").serialize(),
                    type: "PUT",
                    dataType: "json",
                    success: function (response) {

                        if (response.success == true) {
                            notify('Uspeh', 'Proizvod je editovan', 'info');
                            setTimeout(function () {
                                location.reload();
                            }, 1000);

                            $.magnificPopup.close();
                        }
                    },
                    error: function (res) {
                        var errors = res.responseJSON.errors;
                        showErrors(errors);
                    }

                });
            } else {
                showErrors(is_valid_edit.evaluate().errors);
            }
        }
    });
    /*
     Bootstrap Confirmation - CALLBACK
     */
    // delete product
    var x = null;
    $('.delete').click(function () {
        x = $(this);
    });
    $('.delete').confirmation({
        placement: 'left',
        onConfirm: function () {
            $.ajax({
                url: URL + '/main/products/delete',
                data: {
                    currentUrl: window.location.href,
                    id: x.attr('data-number'),
                    search: $('#search').val(),
                    company: $('#company_search').val(),
                },
                type: "DELETE",
                dataType: "json",

                success: function (response) {

                    if (response.success == true) {
                        notify('Uspeh', 'Proizvod je obrisan', 'warning');
                        setTimeout(function () {
                            paginate_handling(response.paginatorData, response.currentUrl, response.filter);
                        }, 1000);

                        $.magnificPopup.close();
                    }
                },
                error: function () {
                    console.log('greska');
                }

            });
        },
        onCancel: function () {

        }
    });
    // search products
    $('#search').keyup(function (e) {
        if (e.which == 13) {
            window.location.href = URL + '/main/products?search=' + $('#search').val() + '&company=' + $('#company_search').val();
        }
    });
    // search on button
    $('#button_search').click(function () {
        window.location.href = URL + '/main/products?search=' + $('#search').val() + '&company=' + $('#company_search').val();
    });
    // search by companies with dropdown
    $('#company_search').on('select2:select', function (e) {
        //console.log(e.params.data);
        if ($('#search').val() === '') {
            window.location.href = URL + '/main/products?company=' + $(this).val();
        } else {
            window.location.href = URL + '/main/products?search=' + $('#search').val() + '&company=' + $(this).val();
        }

    });
});