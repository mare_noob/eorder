//(function ($) {
'use strict';

/**
 * This monitors all AJAX calls that have an error response. If a user's
 * session has expired, then the system will return a 401 status,
 * "Unauthorized", which will trigger this listener and so prompt the user if
 * they'd like to be redirected to the login page.
 */
$(document).ajaxError(function(event, jqxhr, settings, exception) {

    if (exception == 'Unauthorized') {
            // Redirect
            window.location = '/auth/login';
       
    }
});


$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
// email validation
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

// notification function 
function notify(title, message, type, delay = 500) {
    var stack_bar_top = { "dir1": "down", "dir2": "right", "push": "top", "spacing1": 0, "spacing2": 0 };
    var notice = new PNotify({
        title: title,
        text: message,
        type: type,
        addclass: 'stack-bar-top',
        stack: stack_bar_top,
        width: "100%",
        delay: delay,
    });
}
// pagination handling function
/*
* @param paginatorData
* @param curentUrl(only when deleting)
* @param filter
*/
function paginate_handling(paginatorData, currentUrl = null, filter = null) {
    //console.log(paginatorData);
    var first_page_url = paginatorData.first_page_url.replace(/\/create|\/edit|\/delete/gi, '');
    var next_page_url = null;
    var last_page_url = paginatorData.last_page_url.replace(/\/create|\/edit|\/delete|\/search/gi, '');
    var path_url = paginatorData.path.replace(/\/create|\/edit|\/delete|\/search/gi, '');
    var current_page = paginatorData.current_page;
    var last_page = paginatorData.last_page;
    var per_page = paginatorData.per_page;
    var total = paginatorData.total;
    var last_page_number = parseInt(last_page_url.slice(-1));
    if(filter) {
        last_page_url = last_page_url.replace('?', filter);
    }
    
    if (paginatorData.next_page_url !== null) {
        next_page_url = paginatorData.next_page_url.replace(/\/create|\/edit|\/delete|\/search/gi, '');
    }

    //in case of deleting
    if (currentUrl) {
        var currUrlNumber;
        currUrlNumber = parseInt(currentUrl.slice(-1));

        if (currUrlNumber - last_page_number == 1) {
            window.location.href = last_page_url;//+'&search='+$('#search').val();
        } else {
            location.reload();
        }
    } else {
        // if more than per page results
        if (total > per_page) {
            //if result cant fit one pagination redirect to next
            if (total % per_page == 1) {

                window.location.href = last_page_url;

            } else if (total % per_page != 1) {
                // if result is being added from different page redirect
                if (last_page_url != window.location.href) {

                    window.location.href = last_page_url;//+'&search='+$('#search').val();
                } else {
                    location.reload();
                }

            }
        } else {
            location.reload();
        }
    }
}
// convert jquery $().serializeArray() to one object with key: value pairs 
function formDataToArray(param) {
    var result = {};
    $.each(param, function () {
        result[this.name] = this.value;
    });
    return result;
}

// show errors
function showErrors(errors) {
    $('.errors').remove();
    $('.input_error').removeClass('input_error');
    for (var key in errors) {
        if (errors.hasOwnProperty(key)) {
            $('#' + key).addClass('input_error');
            $('#' + key).after('<p class="text-danger errors">' + errors[key] + '</p>');
        }
    }
}

/**
 * Javascript validation
 * first @param formData are values from inputs
 * second @param fields are fields to check and their rulles
 * @example 
 *  fields = { 
 *           'phone': 
 *                 { 
 *                 'required':'', 
 *                  'numeric':'', 
 *                  'min_length':'5' 
 *                  },
 *              'name': 
 *                  {
 *                  'required': '',
 *                  }, 
 *            }
 *if you have field that can be empty but still
 *must be checked if some value is added use 'nullable' rule
 *
 *currently supported rulles are: numeric,required,email,min_length,same and regex
 * 
 * dictionary variable is used to translate fields name in your language
 * 
 * 
 */
function ValidateFields(formData, fields) {
    this.formData = formData;
    this.fields = fields;
    this.dictionary = {
        'name': 'Ime',
        'phone': 'Telefon',
        'address': 'Adresa',
        'logo': 'Slika',
        'image': 'Slika',
        'fname': 'Ime',
        'lname': 'Prezime',
        'username': 'Korisničko ime',
        'password': 'Lozinka',
        'password_confirmation': 'Ponovi lozinku',
        'email': 'Email',
        'TIN': 'PIB',
        'identifier': 'Matični broj',
        'bank_account': 'Žiro račun',
        'price' : 'Cena',
        'trade_discount' : 'Rabat',
        'vat' : 'PDV',
        'city' : 'Grad',
    },
        this.errors = {},
        this.validate = function () {
            for (const key in this.fields) {
                if (this.fields.hasOwnProperty(key)) {
                    for (const subkey in this.fields[key]) {
                        if (this.fields[key].hasOwnProperty(subkey)) {
                            switch (subkey) {
                                case 'required':
                                    if (this.formData[key] === '') {
                                        this.errors[key] = `Polje ${this.dictionary[key]} je obavezno.`;
                                    }
                                    break;
                                case 'numeric':
                                    if (this.fields[key].hasOwnProperty('nullable')) {
                                        if (this.formData[key] !== '') {
                                            if (isNaN(this.formData[key])) {
                                                this.errors[key] = `Polje ${this.dictionary[key]} mora biti broj.`;
                                            }
                                        }
                                    } else {
                                        if (isNaN(this.formData[key])) {
                                            this.errors[key] = `Polje ${this.dictionary[key]} mora biti broj.`;
                                        }
                                    }
                                    break;
                                case 'email':
                                    if (!validateEmail(this.formData[key])) {
                                        this.errors[key] = `${this.dictionary[key]} nije ispravan.`;
                                    }
                                    break;
                                case 'min_length':
                                    if (this.fields[key].hasOwnProperty('nullable')) {
                                        if (this.formData[key] !== '') {
                                            if (Number(this.formData[key].length) <= Number(this.fields[key][subkey])) {
                                                this.errors[key] = `${this.dictionary[key]} mora biti duža od 5 slova.`;
                                            }
                                        }
                                    } else {
                                        if (Number(this.formData[key].length) <= Number(this.fields[key][subkey])) {
                                            this.errors[key] = `${this.dictionary[key]} mora biti duža od 5 slova.`;
                                        }
                                    }
                                    break;
                                case 'same':
                                    if (this.formData[key] !== this.formData[this.fields[key][subkey]]) {
                                        this.errors[key] = `Polje ${this.dictionary[key]} i ${this.dictionary[this.fields[key][subkey]]} moraju da se poklapaju.`;
                                    }
                                    break;
                                case 'regex':
                                    var pattern = new RegExp(this.fields[key][subkey]);
                                    if (this.fields[key].hasOwnProperty('nullable')) {
                                        if (this.formData[key] !== '') {
                                            if (!pattern.test(this.formData[key])) {
                                                this.errors[key] = `${this.dictionary[key]} nije ispravan, format je xxx-xxxx-xx.`;
                                            }
                                        }
                                    } else {
                                        if (!pattern.test(this.formData[key])) {
                                            this.errors[key] = `${this.dictionary[key]} nije ispravan, format je xxx-xxxx-xx.`;
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }


                    }
                }
            }

        }
    this.evaluate = function () {
        if (Object.entries(this.errors).length === 0 && this.errors.constructor === Object) {
            return { 'success': true };
        } else {
            return { 'success': false, 'errors': this.errors };
        }
    }



}


//}).apply(this, [jQuery]);