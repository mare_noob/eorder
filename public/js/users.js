$(document).ready(function () {

    // initialize select2 plugin for user create
    $('#company_id').select2({
        width: '100%',
        // this fixes serach input problem on select2
        dropdownParent: $("#userModal"),

    });

    // clear fields
    function clearFields() {
        $('#uid').val('');
        $('#fname').val('');
        $('#lname').val('');
        $('#email').val('');
        $('#username').val('');
        $('#password').val('');
        $('#password_confirmation').val('');
        // select first element in select2 js plugin
        $('#company_id').val($('#company_id option:first-child').val()).trigger('change');
        $('#gender,select>option:eq(0)').prop('selected', true);
        // remove image from drop field and delete file
        if (document.getElementsByClassName('dz-remove')[0]) {
            document.getElementsByClassName('dz-remove')[0].click();
        }
    }
    // activate add user modal
    $('.userModal').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name',
        modal: true,
        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
            beforeOpen: function () {
                if ($(window).width() < 700) {
                    this.st.focus = false;
                } else {
                    this.st.focus = '#name';
                }
            }
        }
    }).click(function () {
        $('#userModal').find('h2.panel-title').text('Dodaj korisnika');
        $('.modal-confirm').attr('data-type', 'create');
        clearFields();
    });
    // activate edit modal and populate fields
    $('.edit').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name',
        modal: true,
        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
            beforeOpen: function () {
                if ($(window).width() < 700) {
                    this.st.focus = false;
                } else {
                    this.st.focus = '#name';
                }
            }
        }
    }).click(function () {
        $('#userModal').find('h2.panel-title').text('Edituj korisnika');
        $('.modal-confirm').attr('data-type', 'edit');
        var a = $(this).attr('data-number');
        $.ajax({
            url: URL + "/main/users/getData",
            data: { id: a },
            type: "GET",
            dataType: "json",
            success: function (response) {
                if (response.success == true) {

                    var ajaxData = response.data;
                    // populate fields
                    $('#uid').val(ajaxData.id);
                    $('#fname').val(ajaxData.fname);
                    $('#lname').val(ajaxData.lname);
                    $('#email').val(ajaxData.email);
                    $('#username').val(ajaxData.username);
                    // select value in select2 js plugin
                    if (ajaxData.company_id) {
                        $('#company_id').val(ajaxData.company_id).trigger('change');
                    }
                    $('#gender').val(ajaxData.gender);
                    $('#admin_role').val(ajaxData.admin_role);
                    $('#file').val('');
                    $('.modal-confirm').attr('data-type', 'edit');
                }
            },
            error: function () {
                console.log('greska');
            }

        });
    });
    /*
     Modal Dismiss
     */
    $('.modal-dismiss').click(function (e) {
        e.preventDefault();
        $('.errors').remove();
        $('.input_error').removeClass('input_error');
        $('.modal-confirm').attr('data-type', 'create');
        clearFields();
        $.magnificPopup.close();
    });
    /*
     Modal Confirm
     */
    // create user

    $('.modal-confirm').click(function (e) {
        e.preventDefault();
        var formData = formDataToArray($("#formData").serializeArray());
        // validation fields and rulles
        var fields_to_check = {
            'fname': {
                'required': '',
            },
            'lname': {
                'required': '',
            },
            'email': {
                'email': '',
                'required': '',
            },
            'username': {
                'required': '',
            },
            'password': {
                'same': 'password_confirmation',
                'min_length': '5',
                'required': '',
            },
            'password_confirmation': {
                'same': 'password',
            },
        }


        if ($('.modal-confirm').attr('data-type') == 'create') {
            // create data
            // js validation
            var is_valid_create = new ValidateFields(formData, fields_to_check);
            is_valid_create.validate();
            if (is_valid_create.evaluate().success) {
                $.ajax({
                    url: URL + "/main/users/create",
                    data: { formData: formData, search: $('#search').val() },
                    type: "POST",
                    dataType: "json",
                    success: function (response) {
                        if (response.success == true) {
                            notify('Uspeh', 'Korsnik je dodat', 'success');
                            setTimeout(function () {
                                paginate_handling(response.paginatorData, null, response.filter);
                            }, 1000);

                            $.magnificPopup.close();
                        }
                    },
                    error: function (res) {
                        var errors = res.responseJSON.errors;
                        showErrors(errors);
                    }

                });
            } else {
                showErrors(is_valid_create.evaluate().errors);
            }
        } else {
            // edit data
            // js validation
            delete fields_to_check['password']['required'];
            fields_to_check['password']['nullable'] = '';
            var is_valid_edit = new ValidateFields(formData, fields_to_check);
            is_valid_edit.validate();
            if (is_valid_edit.evaluate().success) {
                $.ajax({
                    url: URL + '/main/users/edit',
                    data: $("#formData").serialize(),
                    type: "PUT",
                    dataType: "json",
                    success: function (response) {

                        if (response.success == true) {
                            notify('Uspeh', 'Korsnik je editovan', 'info');
                            setTimeout(function () {
                                location.reload();
                            }, 1000);

                            $.magnificPopup.close();
                        }
                    },
                    error: function (res) {
                        var errors = res.responseJSON.errors;
                        showErrors(errors);
                    }

                });
            } else {
                showErrors(is_valid_edit.evaluate().errors);
            }
        }


    });
    /*
     Bootstrap Confirmation - CALLBACK
     */
    // delete user
    var x = null;
    $('.delete').click(function () {
        x = $(this);
    });
    $('.delete').confirmation({
        placement: 'left',
        onConfirm: function () {
            $.ajax({
                url: URL + '/main/users/delete',
                data: { currentUrl: window.location.href, id: x.attr('data-number'), search: $('#search').val() },
                type: "DELETE",
                dataType: "json",

                success: function (response) {

                    if (response.success == true) {
                        notify('Uspeh', 'Korsnik je obrisan', 'warning');
                        setTimeout(function () {
                            paginate_handling(response.paginatorData, response.currentUrl, response.filter);
                        }, 1000);

                        $.magnificPopup.close();
                    }
                },
                error: function () {
                    console.log('greska');
                }

            });
        },
        onCancel: function () {

        }
    });
    // search users
    $('#search').keyup(function (e) {
        if (e.which == 13) {
            window.location.href = URL + '/main/users?search=' + $(this).val();
        }

    });
    // search on button
    $('#button_search').click(function () {
        window.location.href = URL + '/main/users?search=' + $('#search').val();
    });

    // dropzone
    Dropzone.autoDiscover = false;
    $('#my_dropzone').dropzone({
        url: URL + "/main/users/upload",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        //autoProcessQueue: false,
        maxFiles: 1,
        addRemoveLinks: true,
        acceptedFiles: 'image/*',
        dictInvalidFileType: 'invalid data type',
        init: function () {
            this.on("maxfilesexceeded", function (file) { this.removeFile(file); });

        },
        success: function (file, response) {
            var imgName = response.file_name;
            file.previewElement.classList.add("dz-success");
            console.log("Successfully uploaded :" + imgName);
            $('#file').val(imgName);

            $('.dz-remove').on('click', function () {
                $.ajax({

                    url: URL + "/main/users/upload",
                    data: { delete_file: true, file_name: imgName },
                    type: "GET",
                    dataType: 'json',
                    success: function (response) {
                        $('#file').val('');
                    },
                    error: function (res) {
                        console.log('error');
                    }
                });
            });
        },
        error: function (file, response) {
            file.previewElement.classList.add("dz-error");

            if (response.exception === "Illuminate\\Http\\Exceptions\\PostTooLargeException") {
                $('.dz-error-message').prepend('<p style="width:100%;margin:0 auto" class="text-center">File size too large</p>');
            } else {
                $('.dz-error-message').prepend('<p style="width:100%;margin:0 auto" class="text-center">' + response + '</p>');
            }
            showErrors(response.errors);
            //console.log(response.errors.file[0]);
        }
    }).addClass('dropzone');
});