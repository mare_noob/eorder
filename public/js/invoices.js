$(document).ready(function () {
    var ordersObject = {};
    var total = 0;
    var number_of_clicks = 0;
    // init select2
    $('.custom_select').select2({
        width: '100%',
    });
    /****************** 
     * insert value of by_company to hidden field
     * because its select field is disabled
     * jquery can't pick up value from it
    *******************/
    $('#by_company_id').val($('#company_id option:selected').attr('value'));

    /******************
     * in case of editing fill all necessary fields, and 
     * simulate user actions like on create
     ******************/
    if ($('.save').attr('crud-type') === 'edit_invoice') {
        // disable remove button on edit for all fields
        $('button[id^="remove_product_"]').attr('disabled', 'disabled');
        // foreach every input counter field that has quantity value from db
        $('.edit_invoice').each(function (index, element) {
            // number of clicks on add product button
            number_of_clicks++;
            // id value of product
            var id_value = $(this).attr('data-id');
            var counter_value = parseFloat($(this).val());
            // if counter_value is NaN, default to 1
            if (isNaN(counter_value)) {
                counter_value = parseFloat(1);
            }
            var price_field = $(`#price_${id_value}`);
            // price for one product
            var price_field_value = parseFloat(price_field.attr('data-total')).toFixed(2);
            // new price = counter * price for one product
            var new_price = counter_value * price_field_value;
            new_price = parseFloat(new_price).toFixed(2);
            // insert into <code> tag
            price_field.text(new_price);
            var order_data_element = $(`#order_data_${id_value}`);
            // insert necessary added products data into hidden <p> tag for later use
            order_data_element.attr('data_product_quantity', counter_value).attr('data_product_total_price', new_price);

            // use added products data
            var order_data_element = $('#order_data_' + id_value);
            var data_product_name = order_data_element.attr('data_product_name');
            var data_product_price = order_data_element.attr('data_product_price');
            var data_product_quantity = order_data_element.attr('data_product_quantity');
            var data_product_total_price = order_data_element.attr('data_product_total_price');
            var data_product_original_price = order_data_element.attr('data_product_original_price');
            var data_product_vat = order_data_element.attr('data_product_vat');
            var data_product_trade_discount = order_data_element.attr('data_product_trade_discount');
            total = parseFloat(total) + parseFloat(data_product_total_price);
            total = parseFloat(total).toFixed(2);
            // create object from added products data so it can be easilly inserted into db
            ordersObject[id_value] = {
                'product_id': id_value,
                'quantity': data_product_quantity,
                'price': data_product_original_price,
                'vat': data_product_vat,
                'trade_discount': data_product_trade_discount,
                'total': data_product_price,
            };
            // prepare html to be added to products table
            var subtotal_tbody = `
        <tr id="table_row_${id_value}">
        <td class="number number_of_products_${id_value}">${number_of_clicks}</td>
        <td class="text-weight-semibold text-dark product">${data_product_name}</td>
        <td class="text-center price">${data_product_price} RSD</td>
        <td class="text-center quantity">${data_product_quantity}</td>
        <td class="text-center subtotal_td">${data_product_total_price} RSD</td>
        </tr>`;
            // draw table
            $('.subtotal_tbody').append(subtotal_tbody);

            // insert total value into total table cell
            $('#total_table').text(total + ' RSD');
            // insert total value in hidden total input
            $('#total').val(total);
            // disable add button
            $('#add_product_' + id_value).attr('disabled', 'disabled');
            //enable remove button
            $('#remove_product_' + id_value).removeAttr('disabled');
            // disable spinbox
            $('.counter_value_' + id_value).attr('disabled', 'disabled');


        });
    } else {
        // disable remove button on create for all fields
        $('button[id^="remove_product_"]').attr('disabled', 'disabled');
    }

    // increasing number of products
    $(".spinbox-up").on('click', function () {
        var id_value = $(this).attr("data-id");
        var counter_value = parseFloat($(`#counter_value_${id_value}`).val());
        // if counter_value is NaN, default to 1
        if (isNaN(counter_value)) {
            counter_value = parseFloat(1);
        }
        var price_field = $(`#price_${id_value}`);
        // price for one product
        var price_field_value = parseFloat(price_field.attr('data-total')).toFixed(2);
        // new price counter * price for one product
        var new_price = counter_value * price_field_value;
        new_price = parseFloat(new_price).toFixed(2);
        // insert into code tag
        price_field.text(new_price);
        // fill all data needed for inserting into orders table in db
        var order_data_element = $(`#order_data_${id_value}`);
        // insert necessary added products data into hidden <p> tag for later use
        order_data_element.attr('data_product_quantity', counter_value).attr('data_product_total_price', new_price);


    });
    // decreasing number of products
    $(".spinbox-down").on('click', function () {
        var id_value = $(this).attr("data-id");
        var counter_value = parseFloat($(`#counter_value_${id_value}`).val());
        // if counter_value is NaN, default to 1
        if (isNaN(counter_value)) {
            counter_value = parseFloat(1);
        }
        var price_field = $(`#price_${id_value}`);
        // price for one product
        var price_field_value = parseFloat(price_field.attr('data-total')).toFixed(2);
        // new price counter * price for one product
        var new_price = counter_value * price_field_value;
        new_price = parseFloat(new_price).toFixed(2);
        // insert into code tag
        price_field.text(new_price);
        // fill all data needed for inserting into orders table in db
        var order_data_element = $(`#order_data_${id_value}`);
        // insert necessary added products data into hidden <p> tag for later use
        order_data_element.attr('data_product_quantity', counter_value).attr('data_product_total_price', new_price);
    });
    // when manually inputing numbers
    $('.spinbox-input').keyup(function (event) {
        // if input is not a number
        if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
            event.preventDefault();
        } else {
            var id_value = $(this).attr('data-id');
            var counter_value = parseFloat($(this).val());
            // if counter_value is NaN, default to 1
            if (isNaN(counter_value)) {
                counter_value = parseFloat(1);
            }
            var price_field = $(`#price_${id_value}`);
            // price for one product
            var price_field_value = parseFloat(price_field.attr('data-total')).toFixed(2);
            // new price counter * price for one product
            var new_price = counter_value * price_field_value;
            new_price = parseFloat(new_price).toFixed(2);
            // insert into code tag
            price_field.text(new_price);
            var order_data_element = $(`#order_data_${id_value}`);
            // insert necessary added products data into hidden <p> tag for later use
            order_data_element.attr('data_product_quantity', counter_value).attr('data_product_total_price', new_price);

        }
        /*** Quality of life improvements ****/
        // delete value if value is 1 on counter when clicked on it
    }).on('focusin', function () {
        if ($(this).val() == 1) {
            $(this).val('');
        }
        // return value 1 if there is no value on counter when focused out of the counter field
    }).on('focusout', function () {
        if ($(this).val() == '') {
            $(this).val('1');
        }
    });
    /**
     * Google chrome for mobile, manual input handling.
     * chrome doesnt support jquery keyup event
     */
    $('.spinbox-input').on('input', function () {

    }).on('focusout', function () {
        var id_value = $(this).attr('data-id');
        var counter_value = parseFloat($(this).val());
        // if counter_value is NaN, default to 1
        if (isNaN(counter_value)) {
            counter_value = parseFloat(1);
        }
        var price_field = $(`#price_${id_value}`);
        // price for one product
        var price_field_value = parseFloat(price_field.attr('data-total')).toFixed(2);
        // new price counter * price for one product
        var new_price = counter_value * price_field_value;
        new_price = parseFloat(new_price).toFixed(2);
        // insert into code tag
        price_field.text(new_price);
        var order_data_element = $(`#order_data_${id_value}`);
        // insert necessary added products data into hidden <p> tag for later use
        order_data_element.attr('data_product_quantity', counter_value).attr('data_product_total_price', new_price);
    });


    // adding products for order

    $('button[id^="add_product_"]').on('click', function () {
        // number of clicks on add product button
        number_of_clicks++;
        // id of product
        var id_value = $(this).attr('data-id');
        var order_data_element = $('#order_data_' + id_value);
        var data_product_name = order_data_element.attr('data_product_name');
        var data_product_price = order_data_element.attr('data_product_price');
        var data_product_quantity = order_data_element.attr('data_product_quantity');
        var data_product_total_price = order_data_element.attr('data_product_total_price');
        var data_product_original_price = order_data_element.attr('data_product_original_price');
        var data_product_vat = order_data_element.attr('data_product_vat');
        var data_product_trade_discount = order_data_element.attr('data_product_trade_discount');
        total = parseFloat(total) + parseFloat(data_product_total_price);
        total = parseFloat(total).toFixed(2);
        // create object from added products data so it can be easilly inserted into db
        ordersObject[id_value] = {
            'product_id': id_value,
            'quantity': data_product_quantity,
            'price': data_product_original_price,
            'vat': data_product_vat,
            'trade_discount': data_product_trade_discount,
            'total': data_product_price,
        };
        // prepare html to be added to products table
        var subtotal_tbody = `
        <tr id="table_row_${id_value}">
        <td class="number number_of_products_${id_value}">${number_of_clicks}</td>
        <td class="text-weight-semibold text-dark product">${data_product_name}</td>
        <td class="text-center price">${data_product_price} RSD</td>
        <td class="text-center quantity">${data_product_quantity}</td>
        <td class="text-center subtotal_td">${data_product_total_price} RSD</td>
        </tr>`;
        // draw table
        $('.subtotal_tbody').append(subtotal_tbody);
        // insert total value into total table cell
        $('#total_table').text(total + ' RSD');
        // insert total value in hidden total input
        $('#total').val(total);
        // disable add button
        $(this).attr('disabled', 'disabled');
        //enable remove button
        $('#remove_product_' + id_value).removeAttr('disabled');
        // disable spinbox
        $('.counter_value_' + id_value).attr('disabled', 'disabled');
    });

    // remove products from order
    $('button[id^="remove_product_"]').on('click', function () {
        var id_value = $(this).attr('data-id');
        var order_data_element = $('#order_data_' + id_value);
        var data_product_price = order_data_element.attr('data_product_price');
        var data_product_total_price = order_data_element.attr('data_product_total_price');

        for (var key in ordersObject) {
            if (ordersObject.hasOwnProperty(key)) {
                if (ordersObject[key]['product_id'] === id_value) {
                    delete ordersObject[key];
                }
            }
        }

        //delete ordersObject[number_of_clicks];
        number_of_clicks--;

        $('#table_row_' + id_value).remove();
        total = parseFloat(total) - parseFloat(data_product_total_price);
        total = parseFloat(total).toFixed(2);
        // reset order-data attributes in p hidden tag
        order_data_element.attr('data_product_quantity', '1').attr('data_product_total_price', data_product_price);
        $('#total_table').text(total + ' RSD');
        // insert total value in hidden total input
        $('#total').val(total);
        // disable remove button
        $(this).attr('disabled', 'disabled');
        // enable add button
        $('#add_product_' + id_value).removeAttr('disabled');
        // reset counter
        $('#my_spinbox_' + id_value).spinbox('value', 1);
        // reset value in code element
        $(`#price_${id_value}`).text(parseFloat(data_product_price).toFixed(2));
        // enable spinbox
        $('.counter_value_' + id_value).removeAttr('disabled');

        // keep track of number of items in summary table
        var k = 1;
        $('.number').each(function (index, element) {
            $(this).text(k);
            k++;
        });



    });

    $('.save').click(function (e) {
        e.preventDefault();
        formData = formDataToArray($('#formData').serializeArray());
        if ($('.save').attr('crud-type') === '') {
            $.ajax({
                type: "POST",
                url: URL + "/main/invoices/invoices_create",
                data: {
                    'formData': formData,
                    'ordersData': ordersObject,
                },
                dataType: "JSON",
                success: function (response) {
                    notify('Uspeh', 'Porudžbenica je napravljena', 'success');
                    setTimeout(function () {
                        window.location.href = URL + '/main/invoices/invoices_index';
                    }, 1000);
                },
                error: function (response) {
                    if (response.responseJSON.errors.total[0] === 'Polje total je obavezno.') {
                        notify('Neuspeh', 'Niste uneli nikakav proizvod u porudžbenicu', 'error', 4000);
                    }
                }

            });
        } else if ($('.save').attr('crud-type') === 'edit_invoice') {
            $.ajax({
                type: "PUT",
                url: URL + "/main/invoices/invoices_create/edit",
                data: {
                    'formData': formData,
                    'ordersData': ordersObject,
                },
                dataType: "JSON",
                success: function (response) {
                    notify('Uspeh', 'Porudžbenica je editovana', 'info');
                    setTimeout(function () {
                        window.location.href = URL + '/main/invoices/invoices_index';
                    }, 1000);
                },
                error: function (response) {
                    if (response.responseJSON.errors.total[0] === 'Polje total je obavezno.') {
                        notify('Neuspeh', 'Niste uneli nikakav proizvod u porudžbenicu', 'error', 4000);
                    }
                }

            });
        }


    });
    $('.cancel').click(function (e) {
        e.preventDefault();
        window.location.href = `${URL}/main/invoices/invoices_index`;
    });

    /**
     * Status modal activation
     */
    $('.status').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name',
        modal: true,
        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
            beforeOpen: function () {
                if ($(window).width() < 700) {
                    this.st.focus = false;
                } else {
                    this.st.focus = '#name';
                }
            }
        }
    }).on('click', function () {
        var invoice_id = $(this).attr('data-number');
        $('#invoice_id').val(invoice_id);
        $.ajax({
            type: "GET",
            url: URL + "/main/invoices/invoices_index/getData",
            data: { invoice_id: invoice_id },
            dataType: "json",
            success: function (response) {

                switch (response.status) {
                    case 'processing':
                        $('#processing').prop("checked", true);
                        break;
                    case 'sent':
                        $('#sent').prop("checked", true);
                        break;
                    case 'paid':
                        $('#paid').prop("checked", true);
                        break;
                    default:
                        break;
                }
            },
            error: function (response) {

            }
        });
    });
    /**
     * Modal dissmiss
     */
    $('.modal-dismiss').click(function (e) {
        e.preventDefault();
        $('#invoice_id').val('');
        $.magnificPopup.close();
    });
    /** 
     Modal Confirm
     */
    $('.modal-confirm').click(function (e) {
        e.preventDefault();
        var data = formDataToArray($('#status').serializeArray());
        $.ajax({
            type: "PUT",
            url: URL + "/main/invoices/invoices_index/edit",
            data: {
                formData: data,
                edit_type: 'status',

            },
            dataType: "json",
            success: function (response) {
                location.reload();
            },
            error: function (response) {
                console.log(response);
            }
        });

    });
    /**
     * Delete invoice
     */
    var delete_id = null;
    $('.delete').on('click', function (e) {
        e.preventDefault();
        delete_id = $(this).attr('data-number');
    }).confirmation({
        placement: 'left',
        onConfirm: function () {
            $.ajax({
                type: "DELETE",
                url: URL + "/main/invoices/invoices_index/delete",
                data: {
                    id: delete_id,
                    currentUrl: window.location.href,
                    search: '',
                    company: $('#company_search').val(),
                },
                dataType: "json",
                success: function (response) {
                    if (response.success == true) {
                        notify('Uspeh', 'Porudžbenica je obrisana', 'warning');
                        setTimeout(function () {
                            paginate_handling(response.paginatorData, response.currentUrl, response.filter);
                        }, 1000);

                        $.magnificPopup.close();
                    }
                },
                error: function () {
                    console.log("Error while deleting");
                }
            });

        },
        onCancel: function () {

        }

    });

    // search by companies with dropdown
    $('#company_search').on('select2:select', function (e) {
        //console.log(e.params.data);
        if ($('#search').val() === '') {
            window.location.href = URL + '/main/invoices/invoices_index?company=' + $(this).val();
        } else {
            window.location.href = URL + '/main/invoices/invoices_index?search=' + $('#search').val() + '&company=' + $(this).val();
        }

    });

});