@extends('layouts.master')
@section('title', ("Lista kompanija"))
@section('content')
<header class="page-header">
    <h2>Lista proizvoda</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index.html">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Proizvodi</span></li>
            <li><span>Lista proizvoda</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col-md-12">
        <section class="panel panel-featured panel-featured-primary">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>
                <h2 class="panel-title">Lista proizvoda</h2>
            </header>
            <div class="panel-body" id="table">
                
                <div class="row">
                    <div class="col-xs-6 col-md-3">
                        <a class="productModal btn btn-primary" href="#productModal">Dodaj proizvod</a>
                    </div>
                    <div class="col-xs-6 col-md-2">
                        <div class="input-group mb-md">
                            <span class="input-group-addon btn-primary" id="button_search" style="cursor:pointer"><i aria-hidden="true" class="fa fa-search"></i></span>
                            <input value="<?php echo (isset($search_data) ? $search_data : ''); ?>" name='search' id="search" type="text" class="form-control" placeholder="naziv proizvoda">
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-2">
                        <div class="col-md-12">
                            <select class="form-control mb-md custom_select" name="company_search" id="company_search">
                                <option selected="selected" value=''>Sve kompanije</option>
                                @foreach ($company_data as $item)
                            <option @if(isset($company_search) && $company_search == $item->id) {{'selected=selected'}} @else {{''}} @endif value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-5">
                        <div class="input-group mb-md">
                            
                            <button type="button" id="reset_filters" class="btn btn-primary"><i aria-hidden="true" class="fa fa-remove"></i> Resetuj</button>
                        </div>
                    </div>
                    {{-- <div class="col-xs-0 col-md-0">

                    </div> --}}
                </div>
                <br>
                @if(!$data->isEmpty())
                <div class="table-responsive">
                    <table class="table table-bordered mb-none">
                        <thead>
                            <tr>
                                <th>{{ ("Naziv proizvoda") }}</th>
                                <th>{{ ("Proizvođač") }}</th>
                                <th>{{ ("Cena") }}</th>
                                <th>{{ ("Rabat") }}</th>
                                <th>{{ ("PDV") }}</th>
                                <th>{{ ("Ukupno") }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ ($item->name) }}</td>
                                <td>{{ ($item->company_name) }}</td>
                                <td>{{ ($item->price.' RSD') }}</td>
                                <td>{{ ($item->trade_discount.'%') }}</td>
                                <td>{{ ($item->vat.'%') }}</td>
                                <td>{{ ($item->total.' RSD') }}</td>
                                
                                <td style="min-width: 10%">
                                    <a href="#productModal" class="btn btn-primary edit" data-number="{{ ($item->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;
                                    <a href="javascript:void(0)" class="btn btn-danger delete" data-number="{{ ($item->id) }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @if (isset($search_data) && isset($company_search))
                {{ $data->appends(['search' => $search_data,'company' => $company_search])->links() }}
                @elseif(isset($company_search))
                {{ $data->appends(['company' => $company_search])->links() }}
                @elseif(isset($search_data))
                {{ $data->appends(['search' => $search_data])->links() }}
                @else
                {{ $data->links() }}
                @endif
                
                @else
                <p>No Data<p>
                    @endif
            </div>
        </section>
    </div>
</div>

<div id="productModal" class="modal-block modal-block-primary mfp-hide" >
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">Dodaj proizvod</h2>
        </header>
        <div class="panel-body">
            <form id="formData" class="form-horizontal mb-lg" autocomplete="off">
                {!! csrf_field() !!}
                <input type="text" hidden="true" value="" id="pid" name="id">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="name">{{ ("Naziv proizvoda") }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="name" name="name" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="company_id">Kompanija</label>
                    <div class="col-md-6">
                        <select class="form-control mb-md custom_select" name="company_id" id="company_id">
                            @foreach ($company_data as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="price">{{ ("Cena") }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="price" name="price" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="trade_discount">{{ ("Rabat(%)") }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="trade_discount" name="trade_discount" value="0">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="vat">{{ ("PDV(%)") }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="vat" name="vat" value="0">
                    </div>
                </div>
             </form>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary modal-confirm" data-type="create">{{ ("Sačuvaj") }}</button>
                    <button class="btn btn-default modal-dismiss">{{ ("Otkaži") }}</button>
                </div>
            </div>
        </footer>
    </section>
</div>
@endsection

