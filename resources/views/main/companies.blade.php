@extends('layouts.master')
@section('title', ("Lista kompanija"))
@section('content')
<header class="page-header">
    <h2>Lista kompanija</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index.html">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Kompanije</span></li>
            <li><span>Lista kompanija</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col-md-12">
    {{-- <img src="{{asset('storage/297185.jpg')}}"> --}}
        
        <section class="panel panel-featured panel-featured-primary">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>

                </div>

                <h2 class="panel-title">Lista kompanija</h2>
            </header>
            <div class="panel-body" id="table">
                
                <div class="row">
                    <div class="col-xs-4 col-md-3">
                        <a class="companyModal btn btn-primary" href="#companyModal">Dodaj kompaniju</a>
                    </div>
                    <div class="col-xs-8 col-md-4">
                        <div class="input-group mb-md">
                            <span class="input-group-addon btn-primary" id="button_search" style="cursor:pointer"><i aria-hidden="true" class="fa fa-search"></i></span>
                            <input value="<?php echo (isset($search_data) ? $search_data : ''); ?>" name='search' id="search" type="text" class="form-control" placeholder="Ime">
                        </div>
                    </div>
                    <div class="col-xs-0 col-md-5">

                    </div>
                </div>
                <br>
                @if(!$data->isEmpty())
                <div class="table-responsive">
                    <table class="table table-bordered mb-none">
                        <thead>
                            <tr>
                                <th>{{ ("Ime kompanije") }}</th>
                                <th>{{ ("Grad") }}</th>
                                <th>{{ ("Adresa") }}</th>
                                <th>{{ ("Telefon") }}</th>
                                <th>{{ ("PIB") }}</th>
                                <th>{{ ("Matični broj") }}</th>
                                <th>{{ ("Žiro račun") }}</th>
                                <th>{{ ("U PDV sistemu") }}</th>
                                <th>{{ ("Logo") }}</th>
                                <th>{{ ("Akcije") }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ ($item->name) }}</td>
                                <td>{{ ($item->city) }}</td>
                                <td>{{ ($item->address) }}</td>
                                <td>{{ ($item->phone) }}</td>
                                <td>{{ ($item->TIN) }}</td>
                                @if ($item->identifier !== null)
                                <td>{{ ($item->identifier) }}</td>
                                @else
                                <td>nema podataka</td>
                                @endif
                                @if ($item->bank_account !== null)
                                <td>{{ ($item->bank_account) }}</td>
                                @else
                                <td>nema podataka</td>
                                @endif
                                @if ($item->vat_system)
                                <td class="text-success">Da</td>
                                @else
                                <td class="text-danger">Ne</td>
                                @endif
                                @if ($item->logo !== null)
                                <td><img src="{{asset('storage/'.$item->logo) }}"></td>
                                @else
                                <td>nema logo-a</td>
                                @endif
                                
                                <td style="min-width: 10%">
                                    <a href="#companyModal" class="btn btn-primary edit" data-number="{{ ($item->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;
                                    @if(!Auth::user()->admin_role > 0)
                                    <a href="javascript:void(0)" class="btn btn-danger delete" data-number="{{ ($item->id) }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @if (isset($search_data))
                {{ $data->appends(['search' => $search_data])->links() }}
                @else
                {{ $data->links() }}
                @endif
                
                @else
                <p>No Data<p>
                    @endif
            </div>
        </section>
    </div>
</div>

<div id="companyModal" class="modal-block modal-block-primary mfp-hide">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">Dodaj kompaniju</h2>
        </header>
        <div class="panel-body">
            <form id="formData" class="form-horizontal mb-lg" autocomplete="off" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input type="text" hidden="true" value="" id="cid" name="id">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="name">{{ ("Ime") }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="name" name="name" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="city">{{ ("Grad") }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="city" name="city" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="address">{{ ("Adresa") }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="address" name="address" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="phone">{{ ("Telefon") }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="phone" name="phone" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="TIN">{{ ("PIB") }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="TIN" name="TIN" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="identifier">{{ ("Matični broj") }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="identifier" name="identifier" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="bank_account">{{ ("Žiro račun") }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="bank_account" name="bank_account" value="" placeholder="format je xxx-xxxx-xx">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="vat_system">{{ ("Firma je u PDV sistemu?") }}</label>
                    <div class="col-md-6">
                        <select class="form-control mb-md" id="vat_system" name="vat_system">
                            <option value="0">{{ ("Ne") }}</option>
                            <option value="1">{{ ("Da") }}</option>
                        </select>
                    </div>
                </div>
                {{-- <div class="form-group">
                    <label class="col-md-3 control-label" for="bank_account">{{ ("Žiro račun") }}</label>
                    <div class="col-md-6">
                        <input id="bank_account" name="bank_account" data-plugin-masked-input="" data-input-mask="999-9999999999999-99" placeholder="___-__-____" class="form-control">
                    </div>
                </div> --}}
                <div class="form-group">
                    <label class="col-md-3 control-label" for="logo">{{ ("Logo") }}</label>
                    <div class="col-md-6">
                        <div id="my_dropzone" class=" dz-square dz-clickable">
                            <div class="dz-default dz-message"><span>Drop files here to upload</span>
                            </div>
                        </div>
                    <input hidden='true' type="text" value='' name='logo' id='file'>    
                    </div>
                </div>
             </form>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary modal-confirm" data-type="create">{{ ("Sačuvaj") }}</button>
                    <button class="btn btn-default modal-dismiss">{{ ("Otkaži") }}</button>
                </div>
            </div>
        </footer>
    </section>
</div>
@endsection

