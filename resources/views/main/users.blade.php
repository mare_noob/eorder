@extends('layouts.master')
@section('title', ("Lista korisnika"))
@section('content')
<header class="page-header">
    <h2>{{ ("Lista korisnika") }}</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index.html">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>{{ ("Korisnici") }}</span></li>
            <li><span>{{ ("Lista korisnika") }}</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col-md-12">
        <section class="panel panel-featured panel-featured-primary">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>

                </div>

                <h2 class="panel-title">{{ ("Lista korisnika") }}</h2>
            </header>
            <div class="panel-body" id="table">
                <div class="row">
                    <div class="col-xs-4 col-md-3">
                        <a class="userModal btn btn-primary" href="#userModal">{{ ("Dodaj korisnika") }}</a>
                    </div>
                    <div class="col-xs-8 col-md-4">
                        <div class="input-group mb-md">
                            <span class="input-group-addon btn-primary" id="button_search" style="cursor:pointer"><i aria-hidden="true" class="fa fa-search"></i></span>
                            <input value="<?php echo (isset($search_data) ? $search_data : ''); ?>" name='search' id="search" type="text" class="form-control" placeholder="Ime, prezime, e-mail">
                        </div>
                    </div>
                    <div class="col-xs-0 col-md-5">
                        
                    </div>
                </div>
                <br>
                @if(!$data->isEmpty())
                <div class="table-responsive" id="searchTable">
                    <table class="table table-bordered mb-none">
                        <thead>
                            <tr>
                                <th>{{ ("Ime") }}</th>
                                <th>{{ ("Prezime") }}</th>
                                <th>{{ ("E-mail") }}</th>
                                <th>{{ ("Username") }}</th>
                                <th>{{ ("Kompanija") }}</th>
                                <th>{{ ("Slika") }}</th>
                                <th>{{ ("Akcije") }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ ($item->fname) }}</td>
                                <td>{{ ($item->lname) }}</td>
                                <td>{{ ($item->email) }}</td>
                                <td>{{ ($item->username) }}</td>
                                <td>{{ ($item->company_name) }}</td>
                                @if ($item->image !== null)
                                <td><img src="{{asset('storage/'.$item->image) }}"></td>
                                @else
                                <td>nema slike</td>
                                @endif
                                <td style="min-width: 10%">
                                    <a href="#userModal" class="btn btn-primary edit" data-number="{{ ($item->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;
                                    @if(!Auth::user()->admin_role > 0)
                                    <a href="javascript:void(0)" class="btn btn-danger delete" data-number="{{ ($item->id) }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @if (isset($search_data))
                {{ $data->appends(['search' => $search_data])->links() }}
                @else
                {{ $data->links() }}
                @endif
                
                @else
                <p>No Data<p>
                    @endif
            </div>
        </section>
    </div>
</div>

<div id="userModal" class="modal-block modal-block-primary mfp-hide">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">{{ ("Dodaj korisnika") }}</h2>
        </header>
        <div class="panel-body">
            <form id="formData" class="form-horizontal mb-lg" autocomplete="off">
                {!! csrf_field() !!}
                <input type="text" hidden="true" value="" id="uid" name="id">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="fname">{{ ("Ime") }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="fname" name="fname" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="lname">{{ ("Prezime") }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="lname" name="lname">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="email">{{ ("E-mail") }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="email" name="email">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="username">{{ ("Korisničko ime") }}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="username" name="username">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="password">{{ ("Lozinka") }}</label>
                    <div class="col-md-6">
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="password_confirmation">{{ ("Ponovi lozinku") }}</label>
                    <div class="col-md-6">
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="company_id">Kompanija</label>
                    <div class="col-md-6">
                        <select class="form-control mb-md custom_select" name="company_id" id="company_id">
                            @foreach ($company_data as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="gender">{{ ("Pol") }}</label>
                    <div class="col-md-6">
                        <select class="form-control mb-md" id="gender" name="gender">
                            <option value="m">{{ ("Muški") }}</option>
                            <option value="f">{{ ("Ženski") }}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="admin_role">{{ ("Tip korisnika") }}</label>
                    <div class="col-md-6">
                        <select class="form-control mb-md" id="admin_role" name="admin_role">
                            <option value="3">radnik</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="image">{{ ("Slika") }}</label>
                    <div class="col-md-6">
                        <div id="my_dropzone" class=" dz-square dz-clickable">
                            <div class="dz-default dz-message"><span>Drop files here to upload</span>
                            </div>
                        </div>
                    <input hidden='true' type="text" value='' name='image' id='file'>    
                    </div>
                </div>
            </form>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary modal-confirm" data-type="create">{{ ("Sačuvaj") }}</button>
                    <button class="btn btn-default modal-dismiss">{{ ("Otkaži") }}</button>
                </div>
            </div>
        </footer>
    </section>
</div>
@endsection

