@extends('layouts.master')
@section('title', ("Lista porudžbenica"))
@section('content')
<header class="page-header">
    @if(isset($invoice_name))
    <h2>Napravi porudžbenicu</h2>
    @else
    <h2>Uredi porudžbenicu</h2>
    @endif
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index.html">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Porudžbenice</span></li>
            
            @if(isset($invoice_name))
            <li><span>Napravi porudžbenicu</span></li>
            @else
            <li><span>Uredi porudžbenicu</span></li>
            @endif
        </ol>
        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col-md-12">
        <section class="panel panel-featured panel-featured-primary">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>
                @if(isset($invoice_name))
                <h2 class="panel-title">Napravi porudžbenicu</h2>
                @else
                <h2 class="panel-title">Uredi porudžbenicu</h2>
                @endif
            </header>

            <div class="panel-body">
                @if(isset($invoice_name))
                <h3 class="text-center text-primary hidden-sm hidden-xs">{{$invoice_name}}</h3>
                <h4 class="text-center text-primary  hidden-md hidden-lg hidden-xl">{{$invoice_name}}</h4>
                @else
                <h3 class="text-center text-primary hidden-sm hidden-xs">{{$invoice_data->name}}</h3>
                <h4 class="text-center text-primary  hidden-md hidden-lg hidden-xl">{{$invoice_data->name}}</h4>
                @endif
                <br>
                <div class="col-md-0 col-lg-3"></div>
                <div class="col-md-12 col-lg-6">
                    <form id="formData" class="form-horizontal mb-lg form-bordered col-md-12 col-lg-12" autocomplete="off">
                        {!! csrf_field() !!}
                    <input type="text" hidden="true" value="@if(isset($user_data)){{$user_data->id}}@else{{$invoice_data->user_id}}@endif" id="user_id" name="user_id">
                    <input type="text" hidden="true" value="@if(isset($invoice_name)){{$invoice_name}}@else{{$invoice_data->name}}@endif" id="name" name="name">
                        @if(isset($invoice_data->id))
                        <input type="text" hidden="true" value="{{$invoice_data->id}}" id="invoice_id" name="invoice_id">
                        @endif
                        <input type="text" hidden="true" value="" id="total" name="total">
                        <input type="text" hidden="true" value="" id="by_company_id" name="by_company_id">
                        <div class="form-group">
                            <label class="col-md-3 control-label text-primary" for="company_id">Proizvođač</label>
                            <div class="col-md-6">
                                <select disabled class="form-control mb-md custom_select" name="company_id"
                                    id="company_id">
                                    @foreach ($company_data as $item)
                                    <option @if(isset($user_data->company_id) && $user_data->company_id == $item->id) {{"selected=selected"}}
                                        @elseif(isset($invoice_data->by_company_id) && $invoice_data->by_company_id == $item->id) {{"selected=selected"}} @else @endif value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label text-warning" for="company_id">Kupac</label>
                            <div class="col-md-6">
                                <select class="form-control mb-md custom_select" name="for_company_id" id="for_company_id">
                                    @foreach ($company_data as $item)
                                    <option @if(isset($invoice_data->for_company_id) && $invoice_data->for_company_id == $item->id) {{"selected=selected"}} @else @endif
                                    value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="type">Tip plaćanja</label>
                            <div class="col-md-6">
                                <div class="radio">
                                    <label>
                                        <input id="cash" name="type" type="radio" value="cash" @if(isset($invoice_data->type) && $invoice_data->type === 'cash'){{'checked="true"'}}@endif>
                                        Faktura keš
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input id="bill" name="type" type="radio" value="bill" @if(isset($invoice_data->type) && $invoice_data->type === 'bill'){{'checked="true"'}}@elseif(!isset($invoice_data->type)){{'checked="true"'}}@endif>
                                        Faktura račun
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input id="delivery_note" name="type" type="radio" value="delivery_note" @if(isset($invoice_data->type) && $invoice_data->type === 'delivery_note'){{'checked="true"'}}@endif>
                                        Otpremnica
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form class="form-horizontal mb-lg form-bordered col-md-12 col-lg-12" autocomplete="off">
                    @foreach ($product_data as $item)
                    <?php $temp = null; ?>
                    @if(isset($order_data))
                    
                    @foreach ($order_data as $key => $value)
                    @if($value->product_id == $item->id)
                    <?php
                        $temp = $order_data{$key};
                        unset($order_data{$key}); 
                    ?>
                    @endif
                    @endforeach
                    @endif
                    
                    <div class="form-group">
                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">{{ $item->name }}</label>
                        <div class="col-md-5 col-sm-5 col-xs-6">
                            <div class="spinbox" data-initialize="spinbox" id='my_spinbox_{{$item->id}}'>
                                <div class="input-group" style="">
                                    <input id="counter_value_{{$item->id}}" type="text"
                                        class="spinbox-input form-control counter_value_{{$item->id}} @if($temp !== NULL){{$crud_type}}@endif" maxlength="5" data-id="{{ $item->id }}"
                                    value='@if($temp["product_id"] == $item->id){{$temp["quantity"]}}@else{{'1'}}@endif'>
                                    <div class="spinner-buttons input-group-btn">
                                        <button type="button" class="btn btn-default spinbox-up counter_value_{{$item->id}}" data-id="{{ $item->id }}">
                                            <i class="fa fa-angle-up text-danger"></i>
                                        </button>
                                        <button type="button" class="btn btn-default spinbox-down counter_value_{{$item->id}}" data-id="{{ $item->id }}">
                                            <i class="fa fa-angle-down text-success"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <p>cena <code data-total="{{ $item->total }}" id="price_{{ $item->id }}">{{ $item->total }}</code> RSD</p>
                            <p hidden="true" id="order_data_{{ $item->id }}" data_product_name="{{ $item->name }}" data_product_original_price="{{ $item->price }}" data_product_vat="{{ $item->vat }}" data_product_trade_discount="{{ $item->trade_discount }}" data_product_price="{{ $item->total }}" data_product_quantity="1" data_product_total_price="{{ $item->total }}"></p>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <button data-id="{{ $item->id }}" type="button" class="btn btn-success" id="add_product_{{ $item->id }}">
                                <i class="fa fa-plus"></i>
                            </button>
                            <button data-id="{{ $item->id }}" type="button" class="btn btn-danger" id="remove_product_{{ $item->id }}">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>
                    
                    @endforeach
                    </form>
                    
                </div>
                <div class="col-md-0 col-lg-3"></div>
                <div class="row">
                    <div class="col-md-12 invoice">
                        <div class="table-responsive">
                            <table class="table invoice-items">
                                <thead>
                                    <tr class="h4 text-dark">
                                        <th id="cell-id" class="text-weight-semibold">#</th>
                                        <th id="cell-item" class="text-weight-semibold">Proizvod</th>
                                        <th id="cell-price" class="text-center text-weight-semibold">Cena</th>
                                        <th id="cell-qty" class="text-center text-weight-semibold">Količina</th>
                                        <th id="cell-total" class="text-center text-weight-semibold">Ukupno</th>
                                    </tr>
                                </thead>
                                <tbody class="subtotal_tbody">
                                    
                                </tbody>
                            </table>
                        </div>
                        <div class="invoice-summary">
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-8">
                                    <table class="table h5 text-dark">
                                        <tbody>
                                            <tr class="h4">
                                                <td colspan="2">Ukupno</td>
                                                <td class="text-left" id="total_table"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <button class="btn btn-primary save" crud-type="@if(isset($crud_type)){{$crud_type}}@else{{''}}@endif">{{ ("Sačuvaj") }}</button>
                        <button class="btn btn-default cancel">{{ ("Otkaži") }}</button>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection