@extends('layouts.master')
@section('title', ("Lista porudžbenica"))
@section('content')
<header class="page-header">
    <h2>Lista porudžbenica</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index.html">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Porudžbenice</span></li>
            <li><span>Lista porudžbenica</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
<section class="panel">
    <div class="panel-body">
        <div class="invoice">
            <header class="clearfix">
                <div class="row">
                    <div class="col-sm-6 mt-md">
                    <h3 class="h3 mt-none mb-sm text-dark text-weight-bold ">{{ $invoice_data->name }}</h3>
                    </div>
                    <div class="col-sm-3 mt-md"></div>
                    <div class="col-sm-3 mt-md">
                            <div class="mr-xlg text-right">
                                    <img src="{{URL::asset('assets/images/logo.png')}}" height="80" alt="Zrnoprodukt">
                                </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-md-6">
                                <div class="bill-to">
                                    <p class="h5 mb-xs text-dark text-weight-semibold">Kupac:</p>
                                    <address>
                                        {{ $invoice_data->for_company_name }}
                                    <br>
                                    {{ $invoice_data->for_company_address }}, {{ $invoice_data->for_company_city }}
                                    <br>
                                    Telefon: {{ $invoice_data->for_company_phone }}
                                    <br>
                                    PIB: {{ $invoice_data->for_company_TIN }}
                                    </address>
                                    
                                </div>
                            </div>
                    <div class="col-sm-6 text-right mt-md mb-md">
                        <p class="h5 mb-xs text-dark text-weight-semibold mr-xlg">Prodavac:</p>
                        <address class="ib mr-xlg">
                            {{ $invoice_data->by_company_name }}
                            <br>
                            {{ $invoice_data->by_company_address }}, {{ $invoice_data->by_company_city }}
                            <br>
                            Telefon: {{ $invoice_data->by_company_phone }}
                            <br>
                            PIB: {{ $invoice_data->by_company_TIN }}
                            <br>
                            Žiro račun: <strong>{{ $invoice_data->by_company_bank_account }}</strong>
                            <br>
                            <strong>Datum:</strong> {{ formatDate($invoice_data->created_at, true) }}
                        </address>
                        
                    </div>
                </div>
            </header>
            <div class="table-responsive">
                <table class="table invoice-items">
                    <thead>
                        <tr class="h4 text-dark">
                            <th id="cell-id" class="text-weight-semibold">#</th>
                            <th id="cell-item" class="text-weight-semibold">Proizvod</th>
                            <th id="cell-price" class="text-center text-weight-semibold">Cena</th>
                            <th id="cell-vat" class="text-center text-weight-semibold">PDV</th>
                            <th id="cell-price-vat" class="text-center text-weight-semibold">Cena sa PDV-om</th>
                            <th id="cell-qty" class="text-center text-weight-semibold">Količina</th>
                            <th id="cell-total" class="text-center text-weight-semibold">Ukupno</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; $vat = 0; $price_without_vat = 0; ?>
                        @foreach ($orders_data as $item)
                        <tr>
                            <td>{{ $i }}</td>
                            <td class="text-weight-semibold text-dark">{{ $item->product_name }}</td>
                            <td class="text-center">{{  $item->price }} RSD</td>
                            <td class="text-center">{{  $item->vat }}%</td>
                            <td class="text-center text-weight-semibold text-dark">{{  $item->total }} RSD</td>
                            <td class="text-center">{{ $item->quantity }}</td>
                            <td class="text-center text-weight-semibold text-dark">{{ number_format($item->total * $item->quantity,2) }} RSD</td>
                        </tr>
                        <?php $i++;
                            $vat = $vat + calculateVat($item->price, $item->vat) * $item->quantity;
                            $vat = $vat;
                            if ($item->trade_discount == 0) {
                                $price_without_vat = $price_without_vat + $item->price * $item->quantity;
                                
                            } else {
                                $price_without_vat = $price_without_vat + (($item->price - calculateTradeDiscount($item->price, $item->trade_discount)) * $item->quantity);
                            }
                            
                            $price_without_vat = $price_without_vat;
                        ?>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <?php $vat = $invoice_data->total - $price_without_vat; ?>
            <div class="invoice-summary">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-8">
                        <table style="border: 2px solid black !important; border-collapse: collapse" class="table h5 text-dark custom_summary">
                            <tbody>
                                <tr class="h4">
                                    <td style="border: none;" colspan="2">Cena bez PDV-a</td>
                                    <td class="text-left" style="border: none;">: {{ number_format($price_without_vat, 2) }} RSD</td>
                                </tr>
                                <tr class="h4">
                                    <td colspan="2" style="border: none;">PDV</td>
                                <td class="text-left" style="border: none;">: {{ number_format($vat, 2) }} RSD</td>
                                </tr>
                                <tr class="h4">
                                    <td colspan="2" style="border: none;">Ukupno za uplatu</td>
                                    <td class="text-left" style="border: none;">: {{ number_format($invoice_data->total, 2) }} RSD</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        
        
            <div class="col-md-12" style="border: 2px solid black; color:black;">
                <h4><strong>Napomena o poreskom oslobođenju: Firma @if($invoice_data->for_company_vat_system){{'je'}}@else{{'nije'}}@endif u sistemu PDV-a</strong></h4>
                <h4><strong>FAKTURA/OTPREMNICA JE VAŽEĆA BEZ PEČATA</strong></h4>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6 col-xs-12 text-bold text-dark text-left">
                    <h4 class="col-md-3 col-xs-12" style="border-top: 1px solid black; text-align: center" >(fakturisao)</h4>

                </div>
                <div class="col-md-6 col-xs-12 text-bold text-dark">
                    <h4 class="col-md-3 col-xs-12 pull-right" style="border-top: 1px solid black; text-align: center">(primio)</h4>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="col-lg-7"></div>
        <div class="text-right mr-lg col-lg-4 pull-right">
            
            <div class="col-xs-6 mb-xs col-md-6 col-lg-6 col-xl-4">    
                <a href="{{ URL::route('invoicesIndex')}}" class="btn btn-primary ml-sm"><i class="fa fa-arrow-circle-left"></i> Nazad</a>
            </div>
            <div class="col-xs-6 mb-xs col-md-6 col-lg-6 col-xl-4">
                <a href="{{ URL::route('create_invoices_page', ['invoice_id' => $invoice_data->id]) }}" class="btn btn-info ml-sm"><i class="fa fa-edit"></i> Uredi</a>
            </div>
            <div class="col-xs-12 mb-xs col-md-4 col-lg-6 col-xl-4 pull-right">
                <a href="{{ URL::route('printInvoices', ['invoice_id' => $invoice_data->id]) }}" target="_blank" class="btn btn-primary ml-sm"><i class="fa fa-print"></i> Print</a>
            </div>
        </div>
    </div>
</section>
@endsection