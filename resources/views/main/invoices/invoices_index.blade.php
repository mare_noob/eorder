@extends('layouts.master')
@section('title', ("Lista porudžbenica"))
@section('content')
<header class="page-header">
    <h2>Lista porudžbenica</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index.html">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Porudžbenice</span></li>
            <li><span>Lista porudžbenica</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col-md-12">
        <section class="panel panel-featured panel-featured-primary">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>

                </div>

                <h2 class="panel-title">Lista porudžbenica</h2>
            </header>
            <div class="panel-body" id="table">
                
                <div class="row">
                    <div class="col-xs-12 col-md-2 custom_mbt">
                    <a class="btn btn-primary" href="{{URL::route('create_invoices_page')}}">Napravi Porudžbenicu</a>
                    </div>
                    <div class="col-xs-12 col-md-3 custom_mbt">
                            <label for="comapny_search" class="col-xs-3 col-md-3">Kupac: </label>
                            <div class="col-xs-9 col-md-9">
                                <select class="form-control mb-md custom_select" name="company_search" id="company_search">
                                    <option selected="selected" value=''>Sve kompanije</option>
                                    @foreach ($company_data as $item)
                                    <option @if(isset($company_search) && $company_search == $item->id) {{'selected=selected'}}@endif value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        
                    </div>
                    <div class="col-xs-12 col-md-2 custom_mbt">
                        <div class="input-group mb-md">
                            <span class="input-group-addon btn-primary" id="button_search" style="cursor:pointer"><i aria-hidden="true" class="fa fa-search"></i></span>
                            <input value="<?php echo (isset($search_data) ? $search_data : ''); ?>" name='search' id="search" type="text" class="form-control" placeholder="naziv">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-5 custom_mbt">
                        @if(isset($debt_paid_data))
                        <p class="col-md-12 text-danger" style="font-size: 18px">Duguje: {{$debt_paid_data['debt_cash'].' RSD'}} (keš)<span class="text-dark"> / </span>{{$debt_paid_data['debt_bill']. ' RSD'}} (račun)</p>
                        <p class="col-md-12 text-success" style="font-size: 18px">Plaćeno: {{$debt_paid_data['paid_cash'].' RSD'}} (keš)<span class="text-dark"> / </span>{{$debt_paid_data['paid_bill']. ' RSD'}} (račun)</p>
                        @else
                        <h3 class="col-md-6" style="margin:0 !important;">No data</h3>
                        @endif
                    </div>
                </div>
                <br>
                @if(!$invoice_data->isEmpty())
                <div class="table-responsive">
                    <table class="table table-bordered mb-none">
                        <thead>
                            <tr>
                                <th>{{ ("Naziv") }}</th>
                                <th>{{ ("Korisnik") }}</th>
                                <th>{{ ("Proizvođač") }}</th>
                                <th>{{ ("Kupac") }}</th>
                                <th>{{ ("Ukupno") }}</th>
                                <th>{{ ("Tip") }}</th>
                                <th>{{ ("Stanje") }}</th>
                                <th>{{ ("Napravljena") }}</th>
                                <th>{{ ("Akcije") }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($invoice_data as $item)
                            <tr>
                                <td>{{ ($item->name) }}</td>
                                <td>{{ ($item->user_fname.' '.$item->user_lname) }}</td>
                                <td>{{ ($item->by_company_name) }}</td>
                                <td>{{ ($item->for_company_name) }}</td>
                                <td>{{ ($item->total.' RSD') }}</td>
                                @switch($item->type)
                                    @case('cash')
                                    <td>Faktura keš</td>  
                                        @break
                                    @case('bill')
                                    <td>Faktura račun</td>    
                                        @break
                                    @case('delivery_note')
                                    <td>Otpremnica</td>    
                                        @break
                                    @default
                                @endswitch
                                @switch($item->status)
                                    @case('processing')
                                    <td ><a href="#statusModal" class="status text-light btn btn-danger" data-number="{{ ($item->id) }}" data-value="processing">Obrada</a></td>  
                                        @break
                                    @case('sent')
                                    <td ><a href="#statusModal" class="status text-light bg-info btn btn-info" data-number="{{ ($item->id) }}" data-value="delivered">Isporučen</a></td>    
                                        @break
                                    @case('paid')
                                    <td ><a href="#statusModal" class="status text-light bg-success btn btn-success" data-number="{{ ($item->id) }}" data-value="paid">Plaćeno</a></td>    
                                        @break
                                    @default
                                @endswitch
                                <td>{{ formatDate($item->created_at) }}</td>
                                <td style="min-width: 10%">
                                <a href="{{URL::route('viewInvoices',['invoice_id' => $item->id])}}" class="btn btn-primary view" data-number="{{ ($item->id) }}"><i class="fa fa-newspaper-o" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;
                                    <a href="javascript:void(0)" class="btn btn-danger delete" data-number="{{ ($item->id) }}" data-toggle="confirmation" data-singleton="true"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <p>No Data<p>
                @endif
                @if (isset($search_data) && isset($company_search))
                {{ $invoice_data->appends(['search' => $search_data,'company' => $company_search])->links() }}
                @elseif(isset($company_search))
                {{ $invoice_data->appends(['company' => $company_search])->links() }}
                @elseif(isset($search_data))
                {{ $invoice_data->appends(['search' => $search_data])->links() }}
                @else
                {{ $invoice_data->links() }}
                @endif
                
            </div>
        </section>
    </div>
</div>

<div id="statusModal" class="modal-block modal-block-primary mfp-hide">
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">Promeni status porudžbenice</h2>
        </header>
        <div class="panel-body">
            <div class="modal-wrapper">
                    
                <div class="modal-text">
                    <form id="status">
                        <input type="text" hidden='true' name="invoice_id" id="invoice_id" value="">
                        <div class="form-group">
                                <div class="col-md-4"></div>
                            <div class="col-md-6">
                                <div class="radio">
                                    <label>
                                        <input id="processing" name="status" type="radio" value="processing">
                                        Obrada
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input id="sent" name="status" type="radio" value="sent">
                                        Isporučen
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input id="paid" name="status" type="radio" value="paid">
                                        Plaćeno
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <footer class="panel-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-primary modal-confirm">Confirm</button>
                    <button class="btn btn-default modal-dismiss">Cancel</button>
                </div>
            </div>
        </footer>
    </section>
</div>
       
@endsection