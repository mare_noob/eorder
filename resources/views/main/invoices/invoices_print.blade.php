<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-16"/>
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
    
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/bootstrap/css/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('assets/stylesheets/invoice-print.css')}}" /> 
    
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print</title>
   <style>
        * {
            font-family: DejaVu Sans !important; 
            font-size: 12px;
        }
        .company_data {
            color: #8A8C9A;
        }
        .total_td {
            color: black;
            font-size: 17px;
        }
        .prevent_break {
            page-break-inside: avoid;
        } 
    </style>
</head>
<body>
    <div class="invoice">
        <header class="clearfix">
            {{-- <div class="row">
                <div class="col-sm-3 mt-md"></div>
                <div class="col-sm-6 mt-md">
                <h2 class="h2 mt-none mb-sm text-dark text-weight-bold">{{ $invoice_data->name }}</h2>
                </div>
                <div class="col-sm-3 mt-md">
                        <div class="mr-xlg text-right">
                                <img src="{{URL::asset('assets/images/logo.png')}}" height="80" alt="Zrnoprodukt">
                            </div>
                </div>
            </div> --}}
            <table style="width: 100%">
                <tr>
                    <td class="text-left"><h5 class="h5 mt-none mb-sm text-dark text-weight-bold">{{ $invoice_data->name }}</h5></td>
                    <td class="text-right"><img src="{{URL::asset('assets/images/logo.png')}}" height="80" alt="Zrnoprodukt"></td>
                </tr>
            </table>
                <table style="width: 100%">
                    <tr>
                        <th>Kupac:</th>
                        
                        <th class="text-right">Prodavac:</th>
                    </tr>
                    <tr>
                        <td class="company_data">{{ $invoice_data->for_company_name }}</td>
                        
                        <td class="company_data text-right">{{ $invoice_data->by_company_name }}</td>
                    </tr>
                    <tr>
                        <td class="company_data">{{ $invoice_data->for_company_address }}, {{ $invoice_data->for_company_city }}</td>
                        
                        <td class="company_data text-right">{{ $invoice_data->by_company_address }}, {{ $invoice_data->by_company_city }}</td>
                    </tr>
                    <tr>
                        <td class="company_data">Telefon: {{ $invoice_data->for_company_phone }}</td>
                        
                        <td class="company_data text-right">Telefon: {{ $invoice_data->by_company_phone }}</td>
                    </tr>
                    <tr>
                        <td class="value company_data">PIB: {{ $invoice_data->for_company_TIN }}</td>
                        
                        <td class="company_data text-right">PIB: {{$invoice_data->by_company_TIN }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        
                        <td class="company_data text-right">Žiro račun: <strong>{{ $invoice_data->by_company_bank_account }}</strong></td>
                    </tr>
                    <tr>
                        <td></td>
                        
                        <td class="company_data text-right"><strong>Datum:</strong> {{ formatDate($invoice_data->created_at, true) }}</td>
                    </tr>
                </table>
        </header><br><br>
        
            <table class="table invoice-items" style="width: 100%">
                <thead>
                    <tr class="h4 text-dark">
                        <th id="cell-id" class="text-weight-semibold " style="width: 5% !important">#</th>
                        <th id="cell-item" class="text-weight-semibold " style="width: 25%% !important">Proizvod</th>
                        <th id="cell-item" class=" text-weight-semibold" style="width: 20% !important">Cena</th>
                        <th id="cell-id" class="" style="width: 10% !important">PDV</th>
                        <th id="cell-item" class="text-weight-semibold " style="width: 20% !important">Cena sa PDV-om</th>
                        <th id="cell-qty" class=" text-weight-semibold" >Količina</th>
                        <th id="cell-item" class=" text-weight-semibold" style="width: 25% !important; text-align: center"><strong>Ukupno</strong></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; $vat = 0; $price_without_vat = 0; ?>
                    @foreach ($orders_data as $item)
                    <tr>
                        <td >{{ $i }}</td>
                        <td class="text-weight-semibold text-dark ">{{ $item->product_name }}</td>
                        <td class="">{{ $item->price }} RSD</td>
                        <th class="" style="font-weight: normal">{{ $item->vat }}%</th>
                        <th class="text-weight-semibold ">{{ $item->total }} RSD</th>
                        <td class="text-center" >{{ $item->quantity }}</td>
                        <td class="" style="text-align: center"><strong>{{ number_format($item->total * $item->quantity,2) }} RSD</strong></td>
                    </tr>
                    <?php $i++;
                            $vat = $vat + calculateVat($item->price, $item->vat) * $item->quantity;
                            $vat = $vat;
                            if ($item->trade_discount == 0) {
                                $price_without_vat = $price_without_vat + $item->price * $item->quantity;
                                
                            } else {
                                $price_without_vat = $price_without_vat + (($item->price - calculateTradeDiscount($item->price, $item->trade_discount)) * $item->quantity);
                            }
                            
                            $price_without_vat = $price_without_vat;
                        ?>
                    <?php $i++ ?>
                    @endforeach
                </tbody>
            </table>
            <?php $vat = $invoice_data->total - $price_without_vat; ?>
        <table class="prevent_break" style="margin-left: 220px; min-width: 400px;">
            <tr>
                <td style="border-top:2px solid black; border-left:2px solid black; padding-left: 5px" class="total_td">Cena bez PDV-a</td>
                <td style="border-top:2px solid black; width: 20px;" class="total_td text-center">:</td>
                <td  style="border-top:2px solid black;border-right: 2px solid black" class="total_td">{{ number_format($price_without_vat, 2) }} RSD</td>
            </tr>
            <tr>
                <td style="border-left:2px solid black;padding-left: 5px" class="total_td">PDV</td>
                <td style="width: 20px;" class="total_td text-center">:</td>
                <td style="border-right:2px solid black;" class="total_td">{{ number_format($vat, 2) }} RSD</td>
            </tr>
            <tr>
                <td style="border-bottom:2px solid black; border-left:2px solid black;padding-left: 5px" class="total_td">Ukupno za uplatu</td>
                <td style="width: 20px; border-bottom:2px solid black;" class="total_td text-center" class="total_td text-right">:</td>
                <td style="border-bottom:2px solid black; border-right:2px solid black;" class="total_td">{{ number_format($invoice_data->total, 2) }} RSD</td>
            </tr>
        
        </table>
        <div style="clear:right"></div>
        <br>
        {{-- <div class="invoice-summary">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-8">
                    <table class='table h5 text-dark text-right' >
                        <tbody>
                                <tr class="h4">
                                   
                                    <td class="text-right" style="color:black;font-size:16px">Ukupno &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ $invoice_data->total }} RSD</td>
                                </tr>
                            </tbody>
                        </table>
                </div>
            </div>
        </div> --}}
        <table class="prevent_break" style="width: 100%; border: 3px solid black; border-collapse: collapse;">
            <tr>
                <th></th>
                
            </tr>
            <tr>
                <td >&nbsp;&nbsp;<strong>Napomena o poreskom oslobođenju: Firma @if($invoice_data->for_company_vat_system){{'je'}}@else{{'nije'}}@endif u sistemu PDV-a</strong></td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;<strong>FAKTURA/OTPREMNICA JE VAŽEĆA BEZ PEČATA</strong></td>
            </tr>
        </table>
        <br>
        <br>
        <br>
        <table class="prevent_break" style="width: 100%">
            <tr>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <tr>
                <td style="border-top:1px solid black; text-align: center; width: 200px">(fakturisao)</td>
                <td></td>
                <td class="pull-right" style="border-top:1px solid black; text-align: center;width: 200px">(primio)</td>
            </tr>
        </table>
    </div>
</body>
</html>