<!doctype html>
<html class="fixed">
    <head>
        @include('layouts.head')
        @section('title', "Prijavi se")
        
    </head>
    <body>
        
        <!-- start: page -->
        <section class="body-sign">
            <div class="center-sign">
                <a href="/" class="logo pull-left">
                    <img src="{{URL::asset('assets/images/logo.png')}}" height="54" alt="Zrnoprodukt" />
                </a>

                <div class="panel panel-sign">
                    <div class="panel-title-sign mt-xl text-right">
                        <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> {{"Prijavi se"}}</h2>
                    </div>
                    <div class="panel-body">
                        <form method="POST" action="/auth/login">
                            {!! csrf_field() !!}
                            @if($errors->has('login_fail') > 0)
                            @foreach($errors->get('login_fail') as $error)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                {{ $error }}
                            </div>
                            @endforeach
                            @endif
                            <div class="form-group mb-lg">
                                <label>{{"Korisničko ime"}}</label>
                                <div class="input-group input-group-icon">
                                    <input name="username" type="text" value="{{ old('username') }}" class="form-control input-lg" 
                                           @if($errors->has('username') > 0) style="border-color: red" @endif/>
                                           <span class="input-group-addon">
                                        <span class="icon icon-lg">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </span>
                                </div>
                                @if($errors->has('username') > 0) @foreach($errors->get('username') as $error) <span style="color: red"> {{ $error }} </span> @endforeach @endif
                            </div>

                            <div class="form-group mb-lg">
                                <div class="clearfix">
                                    <label class="pull-left">{{"Lozinka"}}</label>

                                </div>
                                <div class="input-group input-group-icon">
                                    <input name="password" type="password" id="password" class="form-control input-lg" 
                                           @if($errors->has('password') > 0) style="border-color: red" @endif/>
                                           <span class="input-group-addon">
                                        <span class="icon icon-lg">
                                            <i class="fa fa-lock"></i>
                                        </span>
                                    </span>
                                </div>
                                @if($errors->has('password') > 0) @foreach($errors->get('password') as $error) <span style="color: red"> {{ $error }} </span> @endforeach @endif
                            </div>

                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="checkbox-custom checkbox-default">
                                        <input id="RememberMe" name="remember" type="checkbox"/>
                                        <label for="RememberMe">{{"Zapamti me"}}</label>
                                    </div>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <button type="submit" class="btn btn-primary hidden-xs">{{"Prijavi se"}}</button>
                                    <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">{{"Prijavi se"}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <p class="text-center text-muted mt-md mb-md">komaaros&copy; Copyright 2017. All Rights Reserved.</p>
            </div>
        </section>
        <!-- end: page -->
        @include('layouts.footer')
    </body>
</html>