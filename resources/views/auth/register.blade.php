<html>
    <head>
        <title>Register</title>

        <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
        <script
            src="https://code.jquery.com/jquery-2.2.4.js"
            integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
        crossorigin="anonymous"></script>
        <style>
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                height: 100%;
                color: black;
                display: table;
                font-weight: 100;
                font-family: 'sans-serif';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
                margin-bottom: 40px;
            }

            .quote {
                font-size: 24px;
            }
            .form-group {

            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                {{ var_dump($errors->all()) }}
                <div class="title">{{ 'Register' }}</div>

                <div>
                    <form method="POST" action="/auth/register" class="register-form">
                        {{ csrf_field() }}
                        <div>
                            email: <input type="text" value="" name="email">
                        </div>
                        <div>
                            password: <input type="password" value="" name="password">
                        </div>
                        <div>
                            Confirm Password
                            <input type="password" name="password_confirmation">
                        </div>
                        <div>
                            first name: <input type="text" value="" name="firstName">
                        </div>
                        <div>
                            <input type="submit" value="save">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </body>

</html>