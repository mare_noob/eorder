<html>
    <head>
        <title>Success</title>

        <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
        <script
            src="https://code.jquery.com/jquery-2.2.4.js"
            integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
        crossorigin="anonymous"></script>
        <style>
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                height: 100%;
                color: black;
                display: table;
                font-weight: 100;
                font-family: 'sans-serif';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
                margin-bottom: 40px;
            }

            .quote {
                font-size: 24px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">{{ Translit::get('Welcome')}}</div>
                <div class="quote">Welcome {{ $userData->fName . " " . $userData->lName }}</div>
                <form action="/auth/logout" method="GET">
                    <input type="submit" value="Logout">
                </form>
            </div>
        </div>
    </body>
    <script>
        
    </script>
</html>
