</section>
<script type="text/javascript">
    var URL = {!! json_encode(url('/')) !!};
    
    </script>
    <!-- Vendor -->
    <script src="{{URL::asset('assets/vendor/jquery/jquery.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/nanoscroller/nanoscroller.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/magnific-popup/jquery.magnific-popup.js')}}"></script>
    <script src="{{URL::asset('assets/vendor/jquery-placeholder/jquery-placeholder.js')}}"></script>
    <?php if(isset($pageVendorJs)){
        foreach ($pageVendorJs as $v) {
            echo "<script src='".URL::asset($v)."'/></script>"; 
        }
    } ?>
    <!-- Theme Base, Components and Settings -->
    <script src="{{URL::asset('assets/javascripts/theme.js')}}"></script>

    <!-- Theme Custom -->
    <script src="{{URL::asset('assets/javascripts/theme.custom.js')}}"></script>

    <!-- Theme Initialization Files -->
    <script src="{{URL::asset('assets/javascripts/theme.init.js')}}"></script>

    <!-- Main js file -->
    <script src="{{URL::asset('js/main.js')}}"></script>
    
    <!-- Custom JS -->
    <?php if(isset($pageCustomJs)){
        foreach ($pageCustomJs as $v) {
            echo "<script src='".URL::asset($v)."'/></script>"; 
        }
    } ?>
</body>
</html>