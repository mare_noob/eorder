<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title">
            Navigation
        </div>
        <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                    <li class="nav-active">
                        <a href="{{Url::to('index')}}">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-parent">
                        <a>
                            <i class="fa fa-copy" aria-hidden="true"></i>
                            <span>Porudžbenice</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a href="{{ route('invoicesIndex') }}">
                                    Lista porudžbenica
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a>
                            <i class="fa fa-users" aria-hidden="true"></i>
                            <span>{{ "Korisnici" }}</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a href="{{ route("usersIndex")}}">
                                    {{ "Lista korisnika" }}
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a>
                            <i class="fa fa-industry" aria-hidden="true"></i>
                            <span>Kompanije</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a href="{{ route('companiesIndex') }}">
                                    Lista kompanija
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a>
                            <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                            <span>Proizvodi</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a href="{{ route("productsIndex") }}">
                                    Lista proizvoda
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>

        <script>
            // Maintain Scroll Position
            if (typeof localStorage !== 'undefined') {
                if (localStorage.getItem('sidebar-left-position') !== null) {
                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                            sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                    sidebarLeft.scrollTop = initialPosition;
                }
            }
        </script>

    </div>

</aside>
<!-- end: sidebar -->