<!DOCTYPE html>
<html class="fixed">

<head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>Eorder</title>
    <meta name="keywords" content="eorder" />
    <meta name="description" content="eorder">
    <meta name="author" content="komaaros">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--<script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>-->

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light"
        rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/bootstrap/css/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/font-awesome/css/font-awesome.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/magnific-popup/magnific-popup.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css')}}" />

    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/jquery-ui/jquery-ui.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/jquery-ui/jquery-ui.theme.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/morris.js/morris.css')}}" />

    <?php if(isset($pageVendorCss)){
            foreach ($pageVendorCss as $v) {
                echo "<link rel='stylesheet' href='".URL::asset($v)."'/>"; 
            }
        } ?>

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{URL::asset('assets/stylesheets/theme.css')}}" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{URL::asset('assets/stylesheets/skins/default.css')}}" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{URL::asset('assets/stylesheets/theme-custom.css')}}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{URL::asset('css/custom.css')}}" />

    <!-- Head Libs -->
    <script src="{{URL::asset('assets/vendor/modernizr/modernizr.js')}}"></script>

</head>