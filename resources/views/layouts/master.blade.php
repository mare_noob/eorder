@section('head')
@include('layouts.head')
@show
@section('header')
@include('layouts.header')
@show
<div class="inner-wrapper">
    @section('sidebar')
    @include('layouts.sidebar')
    @show
    <section role="main" class="content-body">
    @yield('content')
    </section>
</div>
@section('footer')
@include('layouts.footer')
@show