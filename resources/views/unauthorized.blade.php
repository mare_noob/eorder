@extends('layouts.master')
@section('title', ("Lista porudžbenica"))
@section('content')
<header class="page-header">
    <h2>Nemate dozvolu</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index.html">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Nemate dozvolu</span></li>
            <li><span></span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

<!-- start: page -->
    <section class="body-error error-inside">
        <div class="center-error">

            <div class="row">
                <div class="col-md-8">
                    <div class="main-error mb-xlg">
                        <h2 class="error-code text-dark text-center text-weight-semibold m-none">Nemate dozvolu <i class="fa fa-file"></i></h2>
                        <p class="error-explanation text-center">Nemate dozvolu da dovršite radnju.</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <h4 class="text">Vratite se na početak</h4>
                    <ul class="nav nav-list primary">
                        <li>
                        <a href="{{URL::route("index")}}"><i class="fa fa-caret-right text-dark"></i> Dashboard</a>
                        </li>
                        
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<!-- end: page -->
@endsection                